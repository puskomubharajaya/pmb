<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Change_pass extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_log_maba') != TRUE) {
			echo "<script>alert(Siapa Lu !?);</script>";
			redirect(base_url('board/login/out'),'refresh');
		}
		
	}

	public function index()
	{	
		$data['page'] = 'v_change_form';
		$this->load->view('template',$data);
	}

	function change()
	{
		$log = $this->session->userdata('sess_login_pmb');
		$old_pass = $this->input->post('old_pass');
		$a = crypt(sha1($old_pass), regkey);		
		$cek_pass = $this->db->where('password',$a)->where('email',$log['user'])->get('tbl_user_login',1);
		$count = $cek_pass->num_rows();
		
		if ($count > 0) {
			$new_pass = $this->input->post('new_pass');

			$object = array('password' => crypt(sha1($new_pass), regkey));
			$this->db->where('id', $cek_pass->row()->id);
			$this->db->update('tbl_user_login', $object);
			echo "<script>alert('Berhasil update data');window.location = '".base_url('board/login/out')."';</script>";
		}else{
			echo "<script>alert('Password lama anda salah !!');history.go(-1);</script>";
		}

	}

}

/* End of file Change_pass.php */
/* Location: ./application/controllers/Change_pass.php */