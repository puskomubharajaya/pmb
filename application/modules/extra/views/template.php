<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>e-Registration UBJ</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/logo.ico"/>
    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css">

    <!-- wizard -->
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/wizard/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/wizard/img/favicon.png" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link href="<?php echo base_url(); ?>assets/wizard/css/material-bootstrap-wizard.css" rel="stylesheet" />
    <!-- <link href="<?php echo base_url(); ?>assets/wizard/css/demo.css" rel="stylesheet" /> -->

    <!-- Js -->
    <script src="<?php echo base_url();?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
    <script>window.jQuery || document.write('<script src="<?php echo base_url();?>assets/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
    
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/min/waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>

    <script src="<?php echo base_url();?>assets/js/main.js"></script>

    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-109578352-1', 'auto');
    ga('send', 'pageview');
  </script>

  </head>
  <body>



    <!-- Header Start -->
    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <!-- header Nav Start -->
            <nav class="navbar navbar-default">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#" style="margin-top:-13px;">
                    <img src="<?php echo base_url(); ?>/assets/logoo.png" height="70" width="350" alt="Logo" style="margin-top:-15px" alt="Logo">
                    <h6 style="margin-top:-40px;margin-left:70px;">REGISTRASI ONLINE UBHARAJAYA</h6>
                  </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo base_url();?>">Beranda</a></li>
                    <li><a href="<?php echo base_url();?>main/how_to">Cara Pengisian</a></li>
                    <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                          $log  = $this->session->userdata('sess_login_pmb');
                          $user = $this->temph_model->load_log($log['idus'])->row();
                        ?>
                        <?php echo $user->nm_depan.' '.$user->nm_belakang; ?><span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu">
                          <li><a href="<?php echo base_url('extra/change_pass'); ?>">Rubah Password</a></li>
                          <li><a href="<?php echo base_url('board/login/out'); ?>">Keluar</a></li>                        
                      </ul>
                    </li>
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>
          </div>
        </div>
      </div>
    </header><!-- header close -->
        
    <!-- Wrapper Start -->
    <section id="intro">
        <div class="container">
            <?php $this->load->view($page); ?>
        </div>
    </section>

    
    <!-- footer Start -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <!-- <div class="footer-manu">
              <ul>
                <li><a href="<?php echo base_url();?>main/term">Term & Condition</a></li>
                <li><a target="blank" href="http://ubharajaya.ac.id/wp-ubj/kontak/">Contact</a></li>
              </ul>
            </div> -->
            <p>Copyright &copy; <a href="http://www.ubharajaya.ac.id/">Universitas Bhayangkara Jakarta Raya</a>.</p>
          </div>
        </div>
      </div>
    </footer>

    <!-- wizard -->
    <script src="<?php echo base_url(); ?>assets/wizard/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/wizard/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/wizard/js/jquery.bootstrap.js" type="text/javascript"></script>
    <!--  Plugin for the Wizard -->
    <script src="<?php echo base_url(); ?>assets/wizard/js/material-bootstrap-wizard.js"></script>
    <!--  More information about jquery.validate here: http://jqueryvalidation.org/  -->
    <script src="<?php echo base_url(); ?>assets/wizard/js/jquery.validate.min.js"></script>

            
            
  <!-- <script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script> -->
            
    
    </body>
</html>