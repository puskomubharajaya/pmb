<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>e-Registration UBJ</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/logo.ico"/>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    
    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css">
    
    <!-- Js -->
    <script src="<?php echo base_url();?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
    <script>window.jQuery || document.write('<script src="<?php echo base_url();?>assets/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/min/waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-109578352-1', 'auto');
      ga('send', 'pageview');
    </script>


    <script src="<?php echo base_url();?>assets/js/main.js"></script>


  </head>
  <body>



    <!-- Header Start -->
  <header>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <!-- header Nav Start -->
          <nav class="navbar navbar-default">
            <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                  <img src="<?php echo base_url();?>assets/logoo.png" height="70" width="350" alt="Logo" style="margin-top:-15px">
                </a>
              </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo base_url(); ?>">Beranda</a></li>
                    <li><a href="<?php echo base_url();?>main/how_to">Panduan</a></li>
                    <li><a href="<?php echo base_url('auth/register'); ?>">e-Registrasi</a></li>
                    <!-- <li><a href="<?php //echo base_url();?>auth/main/term">Ketentuan & Kebijakan</a></li> -->
                    <li><a target="blank" href="http://ubharajaya.ac.id/wp-ubj/kontak/">Kontak</a></li>
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>
          </div>
        </div>
      </div>
    </header><!-- header close -->
        
    <!-- Slider Start -->
    <section id="slider">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-2">
            <div class="block">
              <h1 class="animated fadeInUp">PENERIMAAN MAHASISWA BARU<br> UNIVERSITAS BHAYANGKARA</h1>
              <p class="animated fadeInUp">GELOMBANG 1 : 01 November 2019 s/d 11 Januari 2019</p>
              <p class="animated fadeInUp">GELOMBANG 2 : 14 Januari 2019 s/d 22 Maret 2019</p>
              <p class="animated fadeInUp">GELOMBANG 3 : 25 Maret 2019 s/d 24 Mei 2019</p>
              <p class="animated fadeInUp">GELOMBANG 4 : 27 Mei 2019 s/d 02 Agustus 2019</p>
              <hr>
              <p class="animated fadeInUp">Pekan Pengenalan dan Pembekalan Mahasiswa Baru (P3MB) : 23 - 25 Agustus 2019</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Wrapper Start -->
    <section id="intro">
      <div class="container">
        <div class="row">
          <div class="col-md-7 col-sm-12">
            <div class="block">
              <div class="section-title">
                <h2>Tentang Ubhara Jaya</h2>
                <p>Universitas Bhayangkara Jakarta Raya (Ubhara Jaya) sebagai salah satu Perguruan Tinggi Swasta yang berada dibawah pembinaan Yayasan Brata Bhakti sebagai badan penyelenggaranya, berkewajiban mewujudkan tujuan pendidikan nasional dengan Visi dan Misi nya bagi mendukung keberhasilan tugas Kepolisian Negara Republik Indonesia khususnya dan pengembangan kualitas hidup bermasyarakat berbangsa dan benegara pada umumnya.</p>
              </div>
              <p>
              
              </p>
            </div>
          </div><!-- .col-md-7 close -->
          <div class="col-md-5 col-sm-12">
            <div class="block">
              <img src="<?php echo base_url();?>assets/img/wrapper-img.gif" alt="Img">
            </div>
          </div><!-- .col-md-5 close -->
        </div>
      </div>
    </section>

  <!-- <section id="feature">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-6">
          <h2>WE BELIEVE IN GREAT IDEAS</h2>
          <p>Maecenas faucibus mollis interdum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p>Maecenas faucibus mollis interdum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p>Maecenas faucibus mollis interdum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <a href="work.html" class="btn btn-view-works">View Works</a>
        </div>
      </div>
    </div>
  </section> -->
        
    <!-- Service Start -->
    <!-- <section id="service">
      <div class="container">
        <div class="row">
          <div class="section-title">
            <h2>Fakultas & Program Studi</h2>
            <p></p>
          </div>
        </div>
        <div class="row ">
          <div class="col-sm-6 col-md-2">
            <div class="service-item">
              <a href="#ekonomi" data-toggle="modal">
                <i class="icon ion-arrow-graph-up-right"></i>
                <h4>Fakultas Ekonomi</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut </p>
              </a>
            </div>
          </div>
          <div class="col-sm-6 col-md-2">
            <div class="service-item">
              <a href="#teknik" data-toggle="modal">
                <i class="ion-android-settings"></i>
                <h4>Fakultas Teknik</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut </p>
              </a>
            </div>
          </div>
          <div class="col-sm-6 col-md-2">
            <div class="service-item">
              <a href="#fikom" data-toggle="modal">
                <i class="ion-android-chat"></i>
                <h4>Fakultas Komunikasi</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut </p>
              </a>
            </div>
          </div>
          <div class="col-sm-6 col-md-2">
            <div class="service-item">
              <a href="#psikologi" data-toggle="modal">
                <i class="ion-person-stalker"></i>
                <h4>Fakultas Psikologi</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut </p>
              </a>
            </div>
          </div>
          <div class="col-sm-6 col-md-2">
            <div class="service-item">
              <a href="#hukum" data-toggle="modal">
                <i class="ion-ios-infinite"></i>
                <h4>Fakultas Hukum</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut </p>
              </a>
            </div>
          </div>
          <div class="col-sm-6 col-md-2">
            <div class="service-item">
              <a href="#fip" data-toggle="modal">
                <i class="ion-ios-bookmarks"></i>
                <h4>Fakultas Ilmu Pendidikan</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut </p>
              </a>
            </div>
          </div> -->

          <!-- <div class="col-sm-6 col-md-2">
            <div class="service-item">
              <i class="ion-planet"></i>
              <h4>Brand Identity</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut </p>
            </div>
          </div>
          <div class="col-sm-6 col-md-2">
            <div class="service-item">
              <i class="ion-planet"></i>
              <h4>Brand Identity</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut </p>
            </div>
          </div>
          <div class="col-sm-6 col-md-2">
            <div class="service-item">
              <i class="ion-planet"></i>
              <h4>Brand Identity</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut </p>
            </div>
          </div>
          <div class="col-sm-6 col-md-2">
            <div class="service-item">
              <i class="ion-planet"></i>
              <h4>Brand Identity</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut </p>
            </div>
          </div>
          <div class="col-sm-6 col-md-2">
            <div class="service-item">
              <i class="ion-earth"></i>
              <h4>Brand Identity</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut </p>
            </div>
          </div> -->
        <!-- </div>
        <div class="row">
          <center>
          	<div class="col-md-4"></div><div class="col-md-1"></div>
            <div class="col-md-2">
              <div class="service-item">
                <a href="#pasca"data-toggle="modal">
                  <i class="ion-university"></i>
                  <h4>Pasca Sarjana</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut </p>
                </a>
              </div>
            </div>
          </center>
        </div>
      </div>
    </section> -->
    <!-- Call to action Start -->
    <!-- <section id="call-to-action">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="block">
              <h2>We design delightful digital experiences.</h2>
              <p>Read more about what we do and our philosophy of design. Judge for yourself The work and results we’ve achieved for other clients, and meet our highly experienced Team who just love to design.</p>
              <a class="btn btn-default btn-call-to-action" href="#" >Tell Us Your Story</a>
            </div>
          </div>
        </div>
      </div>
    </section> -->
    <!-- Content Start -->
    <!-- <section id="testimonial">
      <div class="container">
        <div class="row">
          <div class="section-title text-center">
            <h2>Testimoni Tentang Ubhara Jaya</h2>
            <p>Kutipan berbagai testimoni tentang Universitas Bhayangkara Jakarta Raya</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="block">
              <ul class="counter-box clearfix">
                <li>
                  <div class="block">
                    <i class="ion-ios-chatboxes-outline"></i>
                    <h4 class="counter">20000</h4>
                    <span>Cups Of Coffee</span>
                  </div>
                </li>
                <li>
                  <div class="block">
                    <i class="ion-ios-glasses-outline"></i>
                    <h4 class="counter">20000</h4>
                    <span>Cups Of Coffee</span>
                  </div>
                </li>
                <li>
                  <div class="block">
                    <i class="ion-ios-compose-outline"></i>
                    <h4 class="counter">20000</h4>
                    <span>Cups Of Coffee</span>
                  </div>
                </li>
                <li>
                  <div class="block">
                    <i class="ion-ios-timer-outline"></i>
                    <h4 class="counter">20000</h4>
                    <span>Cups Of Coffee</span>
                  </div>
                </li>

              </ul>
            </div>
          </div>
          <div class="col-md-6">
            <div class="testimonial-carousel">
              <div id="testimonial-slider" class="owl-carousel">
                <div>
                    <img src="<?php echo base_url();?>assets/img/cotation.png" alt="IMG">
                    <p>"This Company created an e-commerce site with the tools to make our business a success, with innovative ideas we feel that our site has unique elements that make us stand out from the crowd."</p>
                    <div class="user">
                      <img src="<?php echo base_url();?>assets/img/item-img1.jpg" alt="Pepole">
                      <p><span>Rose Ray</span> CEO</p>
                    </div>
                </div>
                <div>
                  <img src="<?php echo base_url();?>assets/img/cotation.png" alt="IMG">
                    <p>"This Company created an e-commerce site with the tools to make our business a success, with innovative ideas we feel that our site has unique elements that make us stand out from the crowd."</p>
                    <div class="user">
                      <img src="<?php echo base_url();?>assets/img/item-img1.jpg" alt="Pepole">
                      <p><span>Rose Ray</span> CEO</p>
                    </div>
                </div>
                <div>
                  <img src="<?php echo base_url();?>assets/img/cotation.png" alt="IMG">
                    <p>"This Company created an e-commerce site with the tools to make our business a success, with innovative ideas we feel that our site has unique elements that make us stand out from the crowd."</p>
                    <div class="user">
                      <img src="<?php echo base_url();?>assets/img/item-img1.jpg" alt="Pepole">
                      <p><span>Rose Ray</span> CEO</p>
                    </div>
                </div>
                <div>
                  <img src="<?php echo base_url();?>assets/img/cotation.png" alt="IMG">
                    <p>"This Company created an e-commerce site with the tools to make our business a success, with innovative ideas we feel that our site has unique elements that make us stand out from the crowd."</p>
                    <div class="user">
                      <img src="<?php echo base_url();?>assets/img/item-img1.jpg" alt="Pepole">
                      <p><span>Rose Ray</span> CEO</p>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> -->

    <!-- ekonomi -->
    <div class="modal fade" id="ekonomi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Prodi Fakultas Ekonomi</h4>
          </div>
          
          <div class="modal-body">  
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#menu1">Akuntansi</a></li>
              <li><a data-toggle="tab" href="#menu2">Manajemen</a></li>
            </ul>

            <div class="tab-content">
              <div id="menu1" class="tab-pane fade in active">
                <h3>HOME</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
              <div id="menu2" class="tab-pane fade">
                <h3>Menu 1</h3>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
            </div>
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          </div>
          
        </div>
      </div>
    </div>

    <!-- teknik -->
    <div class="modal fade" id="teknik" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Prodi Fakultas Teknik</h4>
          </div>
          
          <div class="modal-body">  
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#menu1">Industri</a></li>
              <li><a data-toggle="tab" href="#menu2">Informatika</a></li>
              <li><a data-toggle="tab" href="#menu3">Kimia</a></li>
              <li><a data-toggle="tab" href="#menu4">Lingkungan</a></li>
              <li><a data-toggle="tab" href="#menu5">Perminyakan</a></li>
            </ul>

            <div class="tab-content">
              <div id="menu1" class="tab-pane fade in active">
                <h3>HOME</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
              <div id="menu2" class="tab-pane fade">
                <h3>Menu 1</h3>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
              <div id="menu3" class="tab-pane fade">
                <h3>HOME</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
              <div id="menu4" class="tab-pane fade">
                <h3>Menu 1</h3>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
              <div id="menu5" class="tab-pane fade">
                <h3>HOME</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
            </div>
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          </div>
          
        </div>
      </div>
    </div>

    <!-- fikom -->
    <div class="modal fade" id="fikom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      	<div class="modal-dialog">
       	 	<div class="modal-content">
          		<div class="modal-header">
	              	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	              	<h4 class="modal-title">Prodi Fakultas Komunikasi</h4>
          		</div>
          
	          	<div class="modal-body">  
		            <ul class="nav nav-tabs">
		              	<li class="active"><a data-toggle="tab" href="#menu1">Ilmu Komunikasi</a></li>
		            </ul>

		            <div class="tab-content">
		              <div id="menu1" class="tab-pane fade in active">
		                <h3>HOME</h3>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
		              </div>
		          	</div>
	          	</div>

	          	<div class="modal-footer">
	              	<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	          	</div>
        	</div>
      	</div>
    </div>

    <!-- psikologi -->
    <div class="modal fade" id="psikologi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      	<div class="modal-dialog">
        	<div class="modal-content">
	          	<div class="modal-header">
	              	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	              	<h4 class="modal-title">Prodi Fakultas Psikologi</h4>
	          	</div>
          
	          	<div class="modal-body">  
		            <ul class="nav nav-tabs">
		              	<li class="active"><a data-toggle="tab" href="#menu1">Ilmu Psikologi</a></li>
		            </ul>

		            <div class="tab-content">
		              	<div id="menu1" class="tab-pane fade in active">
			                <h3>HOME</h3>
			                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
		              	</div>
		          	</div>
		        </div>

	          	<div class="modal-footer">
	              	<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	          	</div>
          	</div>
        </div>
      </div>
    </div>

    <!-- hukum -->
    <div class="modal fade" id="hukum" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      	<div class="modal-dialog">
        	<div class="modal-content">
	          <div class="modal-header">
	              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	              <h4 class="modal-title">Prodi Fakultas Hukum</h4>
	          </div>
          
	          <div class="modal-body">  
	            <ul class="nav nav-tabs">
	              <li class="active"><a data-toggle="tab" href="#menu1">Ilmu Hukum</a></li>
	            </ul>

	            <div class="tab-content">
	              <div id="menu1" class="tab-pane fade in active">
	                <h3>HOME</h3>
	                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
	              </div>
	          </div>

	          <div class="modal-footer">
	              <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	          </div>
          	</div>
        </div>
      </div>
    </div>

    <!-- fip -->
    <div class="modal fade" id="fip" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      	<div class="modal-dialog">
        	<div class="modal-content">
	          	<div class="modal-header">
	              	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	              	<h4 class="modal-title">Prodi Fakultas Ilmu Pendidikan</h4>
	          	</div>
          
          		<div class="modal-body">  
		            <ul class="nav nav-tabs">
		              	<li class="active"><a data-toggle="tab" href="#menu1">Olah Raga</a></li>
		              	<li><a data-toggle="tab" href="#menu2">Keguruan</a></li>
		            </ul>

		            <div class="tab-content">
		              <div id="menu1" class="tab-pane fade in active">
		                <h3>HOME</h3>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
		              </div>
		              <div id="menu2" class="tab-pane fade">
		                <h3>HOME</h3>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
		              </div>
	          		</div>
          		</div>

	          	<div class="modal-footer">
	              	<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	          	</div>
        	</div>
      	</div>
    </div>

    <!-- pasca -->
    <div class="modal fade" id="pasca" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      	<div class="modal-dialog">
        	<div class="modal-content">
	          	<div class="modal-header">
	              	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	              	<h4 class="modal-title">Prodi Pasca Sarjana</h4>
	          	</div>
          
	          	<div class="modal-body">  
		            <ul class="nav nav-tabs">
		              <li class="active"><a data-toggle="tab" href="#menu1">Magister Manajemen</a></li>
		              <li><a data-toggle="tab" href="#menu2">Magister Hukum</a></li>
		            </ul>

	            	<div class="tab-content">
		              	<div id="menu1" class="tab-pane fade in active">
			                <h3>HOME</h3>
			                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
		              	</div>
		              	<div id="menu2" class="tab-pane fade">
			                <h3>HOME</h3>
			                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
		              	</div>
		          	</div>
		        </div>

	          	<div class="modal-footer">
	              	<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	          	</div>
          	
        	</div>
      	</div>
    </div>
    
    <!-- footer Start -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <!-- <div class="footer-manu">
              <ul>
                <li><a href="<?php echo base_url();?>">Home</a></li>
                <li><a href="<?php echo base_url();?>main/how_to">How To</a></li>
                <li><a href="#">e-Registration</a></li>
                <li><a href="<?php echo base_url();?>main/term">Term & Condition</a></li>
                <li><a target="blank" href="http://ubharajaya.ac.id/wp-ubj/kontak/">Contact</a></li>
              </ul>
            </div> -->
            <p>Copyright &copy; <a href="http://www.ubharajaya.ac.id">Universitas Bhayangkara Jakarta Raya</a></p>
          </div>
        </div>
      </div>
    </footer>
            
            
  <!-- <script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script> -->
            
    
    </body>
</html>