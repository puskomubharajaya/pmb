<div class="row">
  <div class="col-md-7 col-sm-12">
    <div class="block">
      <div class="section-title">
        <h2>Pendaftaran Mahasiswa Baru Ubhara Jaya</h2>
        <p>Tahun Akademik 2018/2019</p>
      </div>
      <p>
        Berikut langkah-langkah pendaftaran :
        <ul>
            <li><b>1.</b> Membuat Akun Pendaftaran Yang Telah Disediakan.</li>
            <li><b>2.</b> Apabila Verifikasi Akun telah selesai maka link tersebut akan membuka halaman utama pada E-Registration.</li>
            <li><b>3.</b> Pengguna dapat melakukan booking formulir. Apabila tersedia maka pengguna mendapatkan Nomor Booking.</li>
            <li><b>4.</b> Melakukan Pembayaran pada Nomor Rekening Ubhara Jaya melalui ATM maupun Teller.</li>
            <li><b>5.</b> Menunggu Validasi dari BPAK Ubhara Jaya. Jika belum divalidasi maka tidak dapat melengkapi berkas.</li>
            <li><b>6.</b> Apabila berkas telah lengkap maka dapat mencetak Kartu Ujian.</li>
        </ul>
      </p>
      <div class="section-title">
        <h3>e-Registrasi</h3>
        <p>Ketentuan:</p>
      </div>
      <p>
        Berikut langkah-langkah pendaftaran :
        <ul>
            <li><b>-)</b> Jika belum memiliki akun registrasi, maka dapat melakukan pendaftaran akun disamping ini. Jika sudah memiliki akun maka klik sudah mendaftar.</li>
            <li><b>-)</b> Nama harus sesuai dengan akte kelahiran / nomor ujian nasional / ijazah.</li>
            <li><b>-)</b> Harus menggunakan email yang aktif, karena link verifikasi dikirimkan melalui email.</li>
        </ul>
      </p>
    </div>
  </div><!-- .col-md-7 close -->
  <div class="col-md-5 col-sm-12">
    <div class="block">
      <img src="<?php echo base_url();?>assets/bicycle-gif.gif" alt="Img">
    </div>
  </div><!-- .col-md-5 close -->
</div>