<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login_pmb')) {
			redirect(base_url('dashboard'),'refresh'); exit();
		}
	}

	function index()
	{
		$this->load->view('landing_page');
	}

	function how_to()
	{
		$data['page'] = 'v_how';
		$this->load->view('template',$data);
	}

	function term()
	{
		$data['page'] = 'v_term';
		$this->load->view('template',$data);
	}

}