<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Regist extends CI_Controller {

	public function index()
	{
		$data['page'] = 'v_regis';
		$this->load->view('template',$data);
	}

	function registering()
	{
		$cek = $this->temph_model->cek_regist($this->input->post('nik'),$this->input->post('tlp'))->result();
		$tg = date('Y');
		$pecah = substr($tg, 2,4);
		$look= $this->db->query("SELECT count(nik) as poin from tbl_regist where userid like '".$pecah.".Rou%'")->row();
		$no = $look->poin;
		// var_dump($no);exit();
		if($no <= 9){
            $doc = $pecah.".RoU".$this->generateRandomString().".0000".$no;
        }elseif($no <= 99){
            $doc = $pecah.".RoU".$this->generateRandomString().".000".$no;
        }elseif($no <= 999){
            $doc = $pecah.".RoU".$this->generateRandomString().".00".$no;
        }elseif($no <= 9999){
            $doc = $pecah.".RoU".$this->generateRandomString().".0".$no;
        }elseif($no <= 99999){
            $doc = $pecah.".RoU".$this->generateRandomString().".".$no;
        }
		if (count($cek) == 0) {
			$data = array(
				'nm_depan'		=> strtoupper($this->input->post('nm_dpn')),
				'nm_belakang'	=> strtoupper($this->input->post('nm_blk')),
				'nik'			=> $this->input->post('nik'),
				'email'			=> $this->input->post('email'),
				'password'		=> md5(sha1($this->input->post('pass'))),
				'tlp'			=> $this->input->post('tlp'),
				'status'		=> 0,
				'regis_date'	=> date('Y-m-d H:i:s'),
				'userid'		=> $doc
				);
			$this->db->insert('tbl_regist', $data);
			$this->success_page();
		} else {
			echo "<script>alert('Anda Pernah Mendaftar Sebelumnya!');document.location.href='".base_url('main/regist')."'</script>";
		}
	}

	function success_page()
	{
		$this->load->view('template_success_page');
	}

	function generateRandomString() 
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 5; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

}

/* End of file Regist.php */
/* Location: ./application/modules/main/controllers/Regist.php */