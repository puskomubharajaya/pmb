<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['page'] = 'v_regis';
		$this->load->view('template',$data);
	}

	function registering()
	{
		error_reporting(0);
		// cek gelombang
		date_default_timezone_set('Asia/Jakarta');
		$nm_dpn = strtoupper($this->input->post('nm_dpn', TRUE));
		$nm_blk = strtoupper($this->input->post('nm_blk', TRUE));
		$email 	= trim($this->input->post('email', TRUE));
		$pass 	= $this->input->post('password', TRUE);
		$tlp 	= trim($this->input->post('tlp', TRUE));

		$cek = $this->temph_model->cek_regist($email)->result();
		
		if (count($cek) == 0) {
			$gel = $this->temph_model->cekGelombang();
			$akt = $this->temph_model->angkatan();

			// setting untuk menentukan angkatan
			$modify = explode(' - ', $akt['tahun_akademik']);
			$again 	= explode('/', $modify[0]);
			$next 	= substr($again[1], 2,4);
			$exp 	= $next;

			$end = $this->temph_model->lastUser($exp);
			
			if (is_null($end) or !$end or empty($end)) {
				$lastnumber = 0;
			} else {
				$lastnumber = $end;
			}
			
			$baru = (int)substr($lastnumber, -4);
			$c = ($baru+1);
			
			if($baru <= 9){
	            $doc = $exp.$gel."0000".$c;
	        }elseif($baru <= 99){
	            $doc = $exp.$gel."000".$c;
	        }elseif($baru <= 999){
	            $doc = $exp.$gel."00".$c;
	        }elseif($baru <= 9999){
	            $doc = $exp.$gel."0".$c;
	        }elseif($baru <= 99999){
	            $doc = $exp.$gel.$c;
	        }

	        // var_dump($email.'-'.$nm_dpn.'-'.$doc);exit();

	        $data = array(
				'nm_depan'		=> $nm_dpn ,
				'nm_belakang'	=> $nm_blk,
				'password'		=> $pass,
				'email'			=> $email,
				'tlp'			=> $tlp,
				'status'		=> 0,
				'regis_date'	=> date('Y-m-d H:i:s'),
				'userid'		=> $doc,
				'gate'			=> 1
				);
			$this->db->insert('tbl_regist', $data);

	        $this->send_mail($email,$nm_dpn,$doc);

			$m = explode('@', $email);

			redirect(base_url('auth/register/success_page/'.$m[0].'/'.$m[1]),'refresh');
		} else {
			echo "<script>alert('E-Mail/Nomor Telepon sudah terdaftar');document.location.href='".base_url('auth/register')."'</script>";
		}
	}

	function success_page($mail_a,$mail_b)
	{
		$data['email'] = $mail_a.'@'.$mail_b;
		$this->load->view('success_page',$data);
	}

	function send_mail($get_mail,$nama,$userid)
	{		
		//generate key
		$hash = NULL;
        $n = 20; // jumlah karakter yang akan di bentuk.
        $chr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvqxyz0123456789";
        for ($i = 0; $i < $n; $i++) {
            $rIdx = rand(1, strlen($chr));
            $hash .=substr($chr, $rIdx, 1);
        }
        $key = $hash.date('ymdHis');
        //end generate key

        //insert tbl_aktivasi
        $data_mail = array('email' => $get_mail, 
        					'userid' => $userid,
        					'key' => $key,
        					'flag' => 0,
        					'send_date' => date('Y-m-d H:i:s') 
        					);

        $this->db->insert('tbl_aktivasi', $data_mail);

        redirect(base_url('aktivasi/akun/'.$key),'refresh');
        //end insert tbl_aktivasi

		$isi = '<p>Hai '.$nama.',</p>
				<p>Terimakasih telah mendaftarkan diri pada aplikasi Penerimaan Mahasiswa Baru Universitas Bhayangkara Jakarta Raya.</p>
				<p>Untuk memverifikasi akun dan login, silahkan klik LINK di bawah ini.</p>'.anchor('http://pmb.ubharajaya.ac.id/aktivasi/akun/'.$key).'
				</br>
				<p>Untuk Layanan Hubungi infopmb@ubharajaya.ac.id. Contact Person : Nurma (083875082446), Fia (081288808191)</p>
				<p>Terima Kasih</p>';
		
		$judul = 'Aktivasi Akun Calon Mahasiswa';

		$config = array(
	        'protocol' 	=> 'smtp',
	        'smtp_host' => 'tls://smtp.gmail.com',
	        'smtp_port' => 587,
	        'smtp_user' => 'puskom@ubharajaya.ac.id', //email id
	        'smtp_pass' => 'Puskom787566',
	        'mailtype'  => 'html', 
	        'charset'   => 'iso-8859-1'
	    );
	    
	    $this->load->library('email', $config);
	    $this->email->set_newline("\r\n");
	    
	    $this->email->from('infopmb@ubharajaya.ac.id','Universitas Bhayangkara Jakarta Raya');
	    $this->email->to($get_mail); // email array
	    $this->email->subject($judul);   
	    $this->email->message($isi);

	    $result = $this->email->send();

	}


	function generateRandomString() 
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 5; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}



}

/* End of file Regist.php */
/* Location: ./application/modules/main/controllers/Regist.php */