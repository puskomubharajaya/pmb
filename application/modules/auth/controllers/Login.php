<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$data['page'] = 'v_login';
		$this->load->view('template',$data);
	}

	function chk_log()
	{
		$usr = $this->input->post('username', TRUE);
		$pwd = $this->input->post('password', TRUE);
		
		// cek username & password
		$c_akun = $this->temph_model->cek_login($usr,$pwd)->result();

		// cek ketersediaan akun
		$adakah = $this->db->where('email', $usr)->get('tbl_regist')->num_rows();

		// cek aktivasi akun
		$aktiv = $this->temph_model->cek_aktivasi($usr)->row();
		
		if ($adakah > 0) {
			if ($aktiv->flag == 1) {
				if (count($c_akun) == 1) {
			
					foreach ($c_akun as $key) {
						$sess['username'] 	= $key->email;
						$sess['userid'] 	= $key->userid;
						$sess['password'] 	= $key->password;
						$sess['nm_depan'] 	= $key->nm_depan;
						$sess['nm_belakang']= $key->nm_belakang;
						// exit();
						$this->session->set_userdata('sess_login_pmb', $sess);
						redirect(base_url('dashboard'),'refresh');
					}

				} elseif(count($c_akun) == 0) {
					echo "<script>alert('Gagal Login, Pastikan kembali Username dan Password anda sudah benar.');history.go(-1);</script>";exit();
				}
			} else {
				echo "<script>alert('Mohon Verifikasi terlebih dahulu akun anda melalui email yang anda daftarkan.');history.go(-1);</script>";exit();
			}
		} else {
			echo "<script>alert('Gagal Login, Pastikan anda telah mendaftar pada aplikasi ini.');history.go(-1);</script>";exit();	
		}
	}

	function out()
	{
		$this->session->sess_destroy();
		redirect(base_url('auth/login'),'refresh');
	}

	function forgetpass()
	{
		$mail =  $this->input->post('email');
		$phon = $this->input->post('phone');

		$cek = $this->temph_model->chk_account($mail,$phon);
		if (count($cek->result()) > 0) {
			// send e-mail
			$load['data'] = $this->temph_model->load_account($mail)->row();
			$lawn = $this->load->view('v_mail_fgpass', $load, TRUE);
			$this->load->library('email');
			$judul='Universitas Bhayangkara Jakarta Raya';
			$config = array(
	            'protocol' 	=> 'smtp',
	            'smtp_host' => 'ssl://smtp.gmail.com',
	            'smtp_port' => 465,
	            'smtp_user' => 'it@ubharajaya.ac.id',    	//email id
	            'smtp_pass' => 'Pusk0mUBJ',            			// password
	            'mailtype'  => 'html',
	            'charset'   => 'iso-8859-1',
	            'validation'=> TRUE
	        );
	       	$this->email->initialize($config);
	        $this->email->set_newline("\r\n");
	        $this->email->from('it@ubharajaya.ac.id','Universitas Bhayangkara Jakarta Raya');
	        $this->email->to($mail); // email array
	        $this->email->subject($judul);  
	        $this->email->message($lawn);
	        $result = $this->email->send();
	        echo "<script>alert('Mohon cek inbox atau spam pada e-mail Anda!');history.go(-1);</script>";
		} else {
			echo "<script>alert('Akun Tidak Ditemukan!');history.go(-1);</script>";
		}
		
	}

	function changePass()
	{
		$new = $this->input->post('new');
		$old = $this->input->post('old');
		$usr = $this->input->post('userid');

		$this->db->where('userid', $usr);
		$this->db->where('password', md5($old.regkey));
		$ret = $this->db->get('tbl_user_login');

		if ($ret->num_rows() > 0) {
			$arr = array('password' => md5($new.regkey), 'password_plain' => $new);
			$this->db->where('userid', $usr);
			$this->db->update('tbl_user_login', $arr);
			echo '<script>alert("Berhasil");</script>';
			redirect(base_url('auth/login/out'),'refresh');
		} else {
			echo '<script>alert("Password lama salah!");history.go(-1);</script>';
		}
	}

}

/* End of file Login.php */
/* Location: ./application/modules/main/controllers/Login.php */