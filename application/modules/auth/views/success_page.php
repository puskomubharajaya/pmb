<!DOCTYPE html>
<html lang="en">
<head>
  	<title>On-Reg UBJ</title>
  	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/logo.ico"/>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css">
    
    <!-- Js -->
    <script src="<?php echo base_url();?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
    <script>window.jQuery || document.write('<script src="<?php echo base_url();?>assets/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/min/waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>

    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-109578352-1', 'auto');
	  ga('send', 'pageview');
	</script>
</head>
<body>
 
	<div class="container">
		<br><br><br><br>
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="panel panel-success">
				    <div class="panel-heading">
				    	<center>
						    <i class="fa fa-check-circle fa-5x" ></i>
							<h3>Registrasi Berhasil!</h3>
						</center>
				    </div>
				    <div class="panel-body"><p>Untuk verifikasi akun mohon cek inbox atau spam pada e-mail anda kemudian login melalui link yang tertera.

				    	Verifikasi telah kami kirim ke <b> <?php echo $email; ?></b>. <br><br>Terimakasih.<br><br>Salam, <br>Panitia PMB UBJ.</p></div>
				    <div class="panel-footer"><a href="<?php echo base_url('auth/register'); ?>"><i class="fa fa-arrow-left"></i> Kembali</a></div>
			  	</div>
			  	<center>
			  		<a href="http://ubharajaya.ac.id" title=""><small>&copy Universitas Bhayangkara Jakarta Raya</small></a>	
			  	</center>
			</div>
	  	</div>
	</div>

</body>
</html>
