<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-109578352-1', 'auto');
	  ga('send', 'pageview');
	</script>
</head>
<style type="text/css">
	.container {
		width: 45%;
		background: #ecf0f1;
	}
</style>
<body>
	<center>
		<div class="container">
			<img src="http://sia.ubharajaya.ac.id/assets/logo.gif" style="width:7%" alt=""><br>
			<h2>Universitas Bhayangkara Jakarta Raya</h2><br>
			<p>Berikut akun anda untuk <i>Log-In</i> sistem PMB Universitas Bhayangkara Jakarta Raya</p><br>
			<b>
				<p>Username : <?php echo $data->email; ?></p><br>
				<p>Password : <?php echo $data->password; ?></p>
			</b>
			<br>
			<hr style="width:50%">
			<p>&copy; Universitas Bhayangkara Jakarta Raya</p>
		</div>
	</center>
</body>
</html>