<style type="text/css">

  .tabs-left {
    border-bottom: none;
    padding-top: 2px;
    border-right: 1px solid #ddd;
  }

  .tabs-left>li, {
    float: none;
    margin-bottom: 2px;
    margin-right: -1px;
  }

  .tabs-left>li.active>a,
  .tabs-left>li.active>a:hover,
  .tabs-left>li.active>a:focus {
    border-bottom-color: #ddd;
    border-right-color: transparent;
  }

  .tabs-left>li>a {
    border-radius: 4px 0 0 4px;
    margin-right: 0;
    display:block;
  }

  .vertical-text {
    margin-top:50px;
    border: none;
    position: relative;
  }

  .vertical-text>li {
    height: 20px;
    width: 120px;
    margin-bottom: 100px;
  }

  .vertical-text>li>a {
    border-bottom: 1px solid #ddd;
    border-right-color: transparent;
    text-align: center;
    border-radius: 4px 4px 0px 0px;
  }

  .vertical-text>li.active>a,
  .vertical-text>li.active>a:hover,
  .vertical-text>li.active>a:focus {
    border-bottom-color: transparent;
    border-right-color: #ddd;
    border-left-color: #ddd;
  }

  .vertical-text.tabs-left {
    left: -50px;
  }

  .vertical-text.tabs-left>li {
    -webkit-transform: rotate(-90deg);
    -moz-transform: rotate(-90deg);
    -ms-transform: rotate(-90deg);
    -o-transform: rotate(-90deg);
    transform: rotate(-90deg);
  }
</style>

<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-5 col-sm-12" >
    <div class="block" >
      <div class="section-title col-sm-12">
        <h2>Silahkan Log-In</h2>
        <h6>Gunakan akun yang telah dikirim ke e-mail anda</h6>
        <p></p>
      </div>
      <form action="<?php echo base_url('auth/login/chk_log'); ?>" method="post">
        <div class="form-group col-md-12">
          <input type="text" class="form-control" name="username" placeholder="Username" required>
        </div>
        <div class="form-group col-md-12">
          <input type="password" class="form-control" name="password" placeholder="Password" required>
        </div>
        <div class="form-group col-md-4">
          <button type="submit" class="btn" style="background:#DAA520;color:white;"><i class="fa fa-sign-in"></i> Login</button>
          <br><br>
          <a href="#forget" data-toggle="modal">Lupa Password ?</a>
        </div>
      </form>
      <div class="section-title col-sm-12">
        <p></p>
      </div>
    </div>
  </div>
  <!-- .col-md-5 close -->
  
</div>

<div class="modal fade" id="forget" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Masukan E-mail Anda</h4>
            <small>Diperlukan untuk proses perubahan password</small>
        </div>
        <form class ='form-horizontal' action="<?php echo base_url(); ?>auth/login/forgetpass/" method="post">
          <div class="modal-body">  
            <div class="form-group col-md-12">
              <input type="text" class="form-control" name="email" placeholder="Masukan E-mail Anda" required>
            </div>
            <div class="form-group col-md-12">
              <input type="text" class="form-control" name="phone" placeholder="Masukan Telepon Anda" required>
            </div>
          </div> 
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
              <input type="submit" class="btn btn-success" value="Kirim"/>
          </div>
        </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>