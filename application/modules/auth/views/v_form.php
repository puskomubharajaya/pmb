<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/wizard/img/apple-icon.png" />
<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/wizard/img/favicon.png" />
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
<link href="<?php echo base_url(); ?>assets/wizard/css/material-bootstrap-wizard.css" rel="stylesheet" />

<div class="col-sm-8 col-sm-offset-2">
    <!--      Wizard container        -->
    <div class="wizard-container" style="margin-top:-80px">
        <div class="card wizard-card" data-color="green" id="wizardProfile">
            <form action="" method="">
            <!--You can switch " data-color="purple" "  with one of the next bright colors: "green", "orange", "red", "blue"       -->
                <div class="wizard-header">
                    <h3 class="wizard-title">
                       Mohon Lengkapi Formulir Anda
                    </h3>
                    <small>Informasi ini akan digunakan untuk keperluan universitas terhadap calon pendaftar</small>
                </div>
                <div class="wizard-navigation">
                    <ul>
                        <li><a href="#prodi" data-toggle="tab">Pilihan Program Studi</a></li>
                        <li><a href="#about" data-toggle="tab">Data Pribadi</a></li>
                        <li><a href="#account" data-toggle="tab">Data Orang Tua</a></li>
                        <li><a href="#address" data-toggle="tab">Kelengkapan Data</a></li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane" id="prodi">
                      <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">face</i>
                                    </span>
                                    <div class="form-group label-floating">
                                      <label class="control-label">Jenis Pendaftaran<small>(required)</small></label>
                                      <select name="firstname0" class="form-control">
                                        <option value="1">Pilih Jenis Pendaftaran</option>
                                        <option>Mahasiswa Baru</option>
                                        <option>Mahasiswa Konversi</option>
                                      </select>
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">record_voice_over</i>
                                    </span>
                                    <div class="form-group label-floating">
                                      <label class="control-label">Lokasi Kampus<small>(required)</small></label>
                                      <select name="firstname1" class="form-control">
                                        <option value="1">Pilih Lokasi Kampus</option>
                                        <option>Mahasiswa Baru</option>
                                        <option>Mahasiswa Konversi</option>
                                      </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">email</i>
                                    </span>
                                    <div class="form-group label-floating">
                                      <label class="control-label">Program Studi<small>(required)</small></label>
                                      <select name="firstname2" class="form-control">
                                        <option value="1">Pilih Program Studi</option>
                                        <option>Mahasiswa Baru</option>
                                        <option>Mahasiswa Konversi</option>
                                      </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="about">
                      <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">face</i>
                                    </span>
                                    <div class="form-group label-floating">
                                      <label class="control-label">NIK <small>Nomor Induk Kependudukan(required)</small></label>
                                      <input name="firstname" type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">record_voice_over</i>
                                    </span>
                                    <div class="form-group label-floating">
                                      <label class="control-label">Nama Lengkap <small>Sesuai Ijazah Terakhir(required)</small></label>
                                      <input name="lastname" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">email</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Tempat Lahir <small>(required)</small></label>
                                        <input name="tmptlhr" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">email</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Tanggal Lahir <small>(required)</small></label>
                                        <input name="tgllhr" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">email</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Alamat <small>(required)</small></label>
                                        <textarea name="" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">email</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Kode Pos <small>(required)</small></label>
                                        <input name="tgllhr" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">email</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Jenis Kelamin <small>(required)</small></label>
                                        <input type="radio" name="stsb" id="" value="" required> Laki - Laki
                                        <input type="radio" name="stsb" id="" value="" required> Perempuan
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">email</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Kewarganegaraan <small>(required)</small></label>
                                        <input type="radio" name="stsb" id="" value="" required> WNA
                                        <input type="radio" name="stsb" id="" value="" required> WNI
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">email</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Agama <small>(required)</small></label>
                                        <select name="agama" class="form-control">
                                            <option value="1">Pilih Agama</option>
                                            <option>Mahasiswa Baru</option>
                                            <option>Mahasiswa Konversi</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">email</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Status Nikah <small>(required)</small></label>
                                        <input type="radio" name="stsb" id="" value="" required> MENIKAH
                                        <input type="radio" name="stsb" id="" value="" required> JANDA/DUDA
                                        <input type="radio" name="stsb" id="" value="" required> BELUM MENIKAH
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">email</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Bekerja <small>(required)</small></label>
                                        <input type="radio" name="stsb" id="" value="" required> YA
                                        <input type="radio" name="stsb" id="" value="" required> TIDAK
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="account">
                        <h4 class="info-text"> What are you doing? (checkboxes) </h4>
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="col-sm-4">
                                    <div class="choice" data-toggle="wizard-checkbox">
                                        <input type="checkbox" name="jobb" value="Design">
                                        <div class="icon">
                                            <i class="fa fa-pencil"></i>
                                        </div>
                                        <h6>Design</h6>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="choice" data-toggle="wizard-checkbox">
                                        <input type="checkbox" name="jobb" value="Code">
                                        <div class="icon">
                                            <i class="fa fa-terminal"></i>
                                        </div>
                                        <h6>Code</h6>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="choice" data-toggle="wizard-checkbox">
                                        <input type="checkbox" name="jobb" value="Develop">
                                        <div class="icon">
                                            <i class="fa fa-laptop"></i>
                                        </div>
                                        <h6>Develop</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="address">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="info-text"> Are you living in a nice area? </h4>
                            </div>
                            <div class="col-sm-7 col-sm-offset-1">
                                <div class="form-group label-floating">
                                    <label class="control-label">Street Name</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">Street Number</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group label-floating">
                                    <label class="control-label">City</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group label-floating">
                                    <label class="control-label">Country</label>
                                    <select name="country" class="form-control">
                                        <option disabled="" selected=""></option>
                                        <option value="Afghanistan"> Afghanistan </option>
                                        <option value="Albania"> Albania </option>
                                        <option value="Algeria"> Algeria </option>
                                        <option value="American Samoa"> American Samoa </option>
                                        <option value="Andorra"> Andorra </option>
                                        <option value="Angola"> Angola </option>
                                        <option value="Anguilla"> Anguilla </option>
                                        <option value="Antarctica"> Antarctica </option>
                                        <option value="...">...</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wizard-footer">
                    <div class="pull-right">
                        <input type='button' class='btn btn-next btn-fill btn-success btn-wd' name='next' value='Next' />
                        <input type='button' class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='Finish' />
                    </div>

                    <div class="pull-left">
                        <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' />
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div> <!-- wizard container -->
</div>

<script src="<?php echo base_url(); ?>assets/wizard/js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/wizard/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/wizard/js/jquery.bootstrap.js" type="text/javascript"></script>
<!--  Plugin for the Wizard -->
<script src="<?php echo base_url(); ?>assets/wizard/js/material-bootstrap-wizard.js"></script>
<!--  More information about jquery.validate here: http://jqueryvalidation.org/  -->
<script src="<?php echo base_url(); ?>assets/wizard/js/jquery.validate.min.js"></script>