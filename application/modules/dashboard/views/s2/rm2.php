<div id="reads2">
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">credit_card</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">NPM Lama</label>
                <input class="form-control span4" type="text" name="npmsatu">
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">today</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">Tahun Masuk di UBJ</label>
                <input class="form-control span4" type="text" name="tahunmasuks2">
            </div>
        </div>
    </div>
</div>