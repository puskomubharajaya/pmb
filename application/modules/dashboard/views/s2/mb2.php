<div id="mabas2">
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">domain</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">Asal Universitas</label>
                <input class="form-control span4" type="text" name="asal_sch" required>
            </div>
        </div>
    </div>
</div>