<div id="konvs2">
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">domain</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">Jenis Perguruan Tinggi</label>
                <input type="radio" name="jenis_skl" value="NGR"  required> NEGERI &nbsp;&nbsp;&nbsp;
                <input type="radio" name="jenis_skl" value="SWT"> SWASTA
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">domain</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">Nama Perguruan Tinggi</label>
                <input class="form-control span4" type="text" name="asal_pt" required>
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">local_library</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">Program Studi</label>
                <input class="form-control span4" type="text" name="prodi_pt" required>
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">today</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">Tahun lulus/Semester</label>
                <input class="form-control span4" type="text" name="lulus_pt" required>
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">map</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">Kota Asal PTS/PTN</label>
                <input class="form-control span4" type="text" name="kota_pt" required>
            </div>
        </div>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">credit_card</i>
            </span>
            <div class="form-group label-floating">
                <label class="control-label">NPM/NIM</label>
                <input class="form-control span4" type="text" name="npm_pt" required>
            </div>
        </div>
    </div>
</div>