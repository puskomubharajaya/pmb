<script type="text/javascript">
    $(document).ready(function () {
        $(window).on('load',function(){
            $('#myModal').modal('show');
        });

        $(".close").click(function() {
            window.location.href = '<?php echo base_url('dashboard'); ?>';
        });
    });
</script>

<div class="col-md-12 alert alert-warning alert-dismissible fade in" role="alert">
    <strong>
        Anda mengisi lebih dari satu formulir, oleh karena itu diharuskan memilih program studi terlebih dahulu sebelum mengisi kelengkapan berkas. 
        Silahkan <i><u>reload</u></i> halaman ini atau kembali ke <a href="<?php echo base_url('dashboard') ?>" title="">Dashboard</a>
    </strong>.
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle"></span></button>
                <h3 class="modal-title" id="myModalLabel">Pilih Formulir Untuk Pengisian Berkas</h3>
            </div>
            <div class="modal-body">
                <div class="modal-form">
                    <p>Anda mengisi lebih dari satu formulir, oleh karena itu diharuskan memilih program studi terlebih dahulu sebelum mengisi kelengkapan berkas.</p>
                    <form role="form" action="<?php echo base_url(); ?>dashboard/berkas/postFormModalFile" method="post">
                        <div class="form-group">
                            <select class="form-control" name="optprodi">
                                <option disabled="" selected="">-- Pilih Prodi --</option>
                                <?php foreach ($forkey as $rows) { ?>
                                    <option value="<?php echo $rows->key_booking; ?>"><?php echo get_prodi($rows->prodi); ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="clearfix">
                            <button type="submit" class="btn  btn-success pull-left">Submit</button>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>