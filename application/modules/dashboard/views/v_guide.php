<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Panduan E-Registrasi</a></li>
    <li><a data-toggle="tab" href="#menu1">Daftar Harga</a></li>
    <li><a data-toggle="tab" href="#menu2">Berkas Kelengkapan</a></li>
</ul>
<div class="tab-content">
    <div id="home" class="tab-pane fade in active">
    	<br>
    	<embed src="<?php echo base_url('assets/panduan_ereg.pdf') ?>" width="1200" height="1200" ></embed>
    </div>
    <div id="menu1" class="tab-pane fade">
    	<br>
    	<embed src="<?php echo base_url('assets/scan0003.jpg') ?>" width="1200"></embed>
    </div>
    <div id="menu2" class="tab-pane fade">
        <br>
        <div class="row">
            <div class="col-md-6">
                <strong>Kelengkapan Untuk Sarjana (S1 - Mahasiswa Baru)</strong>
                <hr>
                <table class="table table-stripped">
                    <thead>
                        <tr>
                            <td>No</td>
                            <td>Kelengkapan</td>
                            <td>Keterangan</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Fotocopy Ijazah</td>
                            <td>3 lembar yang telah dilegalisir (Asli)</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Fotocopy SKHUN</td>
                            <td>3 lembar yang telah dilegalisir (Asli)</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Fotocopy Surat Kelulusan</td>
                            <td>3 lembar</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Fotocopy KTP / KTA (khusus POLRI)</td>
                            <td>3 lembar</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Fotocopy Kartu Keluarga</td>
                            <td>3 lembar</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Fotocopy Akta Kelahiran</td>
                            <td>3 lembar</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>Materai 6000</td>
                            <td>1 (satu)</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>Foto (kemeja putih)</td>
                            <td>2x3 (4 lembar), 3x4 (6 lembar)</td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>Fotocopy Rapot Halaman dapan & Nilai Akhir</td>
                            <td>3 lembar yang telah dilegalisir (Asli)</td>
                        </tr>
                    </tbody>
                </table>  
            </div>
            <div class="col-md-6">
                <strong>Kelengkapan Untuk Sarjana (S1 - Konversi)</strong>
                <hr>
                <table class="table table-stripped">
                    <thead>
                        <tr>
                            <td>No</td>
                            <td>Kelengkapan</td>
                            <td>Keterangan</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Fotocopy Ijazah</td>
                            <td>3 lembar yang telah dilegalisir</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Fotocopy SKHUN</td>
                            <td>3 lembar yang telah dilegalisir</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Fotocopy Surat Pindah</td>
                            <td>3 lembar</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Fotocopy KTP / KTA (khusus POLRI)</td>
                            <td>3 lembar</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Fotocopy Kartu Keluarga</td>
                            <td>3 lembar</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Fotocopy Akta Kelahiran</td>
                            <td>3 lembar</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>Materai 6000</td>
                            <td>1 (satu)</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>Foto (kemeja putih)</td>
                            <td>2x3 (4 lembar), 3x4 (6 lembar)</td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>Sertifikat Akreditasi</td>
                            <td>3 lembar</td>
                        </tr>
                    </tbody>
                </table> 
            </div>
        </div>
        <hr><hr>
        <div class="row">
            <div class="col-md-6">
                <strong>Kelengkapan Untuk Pascasarjana (S2 - Mahasiswa Baru)</strong>
                <hr>
                <table class="table table-stripped">
                    <thead>
                        <tr>
                            <td>No</td>
                            <td>Kelengkapan</td>
                            <td>Keterangan</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Fotocopy Ijazah</td>
                            <td>3 lembar yang telah dilegalisir</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Fotocopy Transkrip</td>
                            <td>3 lembar yang telah dilegalisir</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Fotocopy Surat Kelulusan</td>
                            <td>3 lembar</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Fotocopy KTP / KTA (khusus POLRI)</td>
                            <td>3 lembar</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Fotocopy Kartu Keluarga</td>
                            <td>3 lembar</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Fotocopy Akta Kelahiran</td>
                            <td>3 lembar</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>Materai 6000</td>
                            <td>1 (satu)</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>Foto (kemeja putih)</td>
                            <td>2x3 (4 lembar), 3x4 (6 lembar)</td>
                        </tr>
                    </tbody>
                </table>  
            </div>
            <div class="col-md-6">
                <strong>Kelengkapan Untuk Pascasarjana (S2 - Konversi)</strong>
                <hr>
                <table class="table table-stripped">
                    <thead>
                        <tr>
                            <td>No</td>
                            <td>Kelengkapan</td>
                            <td>Keterangan</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Fotocopy Ijazah</td>
                            <td>3 lembar yang telah dilegalisir</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Fotocopy Transkrip</td>
                            <td>3 lembar yang telah dilegalisir</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Fotocopy Surat Pindah</td>
                            <td>3 lembar</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Fotocopy KTP / KTA (khusus POLRI)</td>
                            <td>3 lembar</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Fotocopy Kartu Keluarga</td>
                            <td>3 lembar</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Fotocopy Akta Kelahiran</td>
                            <td>3 lembar</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>Materai 6000</td>
                            <td>1 (satu)</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>Foto (kemeja putih)</td>
                            <td>2x3 (4 lembar), 3x4 (6 lembar)</td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>Sertifikat Akreditasi</td>
                            <td>3 lembar</td>
                        </tr>
                    </tbody>
                </table> 
            </div>
        </div>
    </div>
</div>