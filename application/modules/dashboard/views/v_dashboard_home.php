<?php $sess = $this->session->userdata('sess_login_pmb'); ?>

<!-- show hide berkas panel -->
<script type="text/javascript">
    $(document).ready(function() {
        $.ajax({

            url: "<?php echo base_url('dashboard/cekform/'); ?>",

            type: "post",

            success: function(d) {

                var parse = JSON.parse(d);

                if (parse === 0) {
                    $('#berkas').remove();
                } else {
                    $('#berkas').show();
                }
            }
        });
    });
</script>

<script type="text/javascript">
    function parseKey(key)
    {
        $("#contMods").load('<?php echo base_url('dashboard/booking_form/loadModalPayment/') ?>'+key);
    }
</script>

<style type="text/css">
    .center-pos {
        text-align: center;
        vertical-align: middle;
    }
</style>

<!--start page content-->
<div class="row">
    <div class="col-md-8">
        <div class="col-md-12">
            <div class="panel panel-warning collapsed">
                <div class="panel-heading">
                    Riwayat Pembelian Formulir
                </div>
                <div class="panel-body">
                    <table class="table table-bordered"  id="">
                        <thead>
                            <tr>
                                <th class="center-pos">ID Booking</th>
                                <th class="center-pos">Program Studi</th>
                                <th class="center-pos">Tanggal Booking</th>
                                <th class="center-pos">Batas Waktu Pembayaran</th>
                                <th class="center-pos">Status Booking</th>
                                <th class="center-pos">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $jika = count($load);
                            if ($jika < 1) { ?>
                                <tr>
                                    <td colspan="6">Belum ada data</td>
                                </tr>
                            <?php } else {

                                foreach ($load as $val) { ?>
                                    <tr>
                                        <td><?php echo $val->key; ?></td>
                                        <td><?php echo get_prodi($val->prodi); ?></td>
                                        <td><?php echo $val->bookdate; ?></td>
                                        <td>
                                            <!-- fungsi tambah tanggal -->
                                            <?php
                                                date_default_timezone_set('Asia/Jakarta');
                                                $date = new DateTime($val->bookdate);
                                                $date->add(new DateInterval('P1D'));
                                                echo $date->format('Y-m-d') . "\n";
                                            ?>
                                            <!-- end fungsi tambah tanggal -->
                                        </td>
                                        <td>
                                            <?php if (is_null($val->valid) || $val->valid == '') {
                                                echo "Anda belum melakukan konfirmasi pembayaran";
                                            } elseif ($val->valid == 1) {
                                                echo "Menunggu validasi BPAK";
                                            } elseif ($val->valid == 2) {
                                                echo "Pembayaran tervalidasi";
                                            } elseif ($val->valid == 3) {
                                                echo "Anda membatalkan pemesanan";
                                            } ?>
                                        </td>
                                        <td style="text-align: center" width="120">
                                            <?php if (is_null($val->valid)) { ?>

                                                <a class="btn btn-success" 
                                                    data-toggle="modal" 
                                                    data-target="#loginModal" 
                                                    onclick="parseKey('<?php echo $val->key; ?>')" 
                                                    title="Konfirmasi Pembayaran"><i class="fa fa-money"></i>
                                                </a>
                                                <a href="<?= base_url('dashboard/booking_form/voidBook/'.$val->key) ?>" 
                                                    onClick="return confirm('Anda ingin membatalkan pemesanan?')" title="Batalkan Pemesanan" 
                                                    class="btn btn-warning"><i class="fa fa-times"></i>
                                                </a>

                                            <?php } elseif ($val->valid == 1) { ?>

                                                <button class="btn btn-primary" 
                                                        onclick="alert('Mohon tunggu validasi dari BPAK UBJ')">
                                                        <i class="fa fa-info"></i>
                                                </button>

                                            <?php } elseif ($val->valid == 2) { ?>

                                                <a class="btn btn-info" href="<?php echo base_url('dashboard/formulir/'.myEncode($val->prodi,TRUE).'/'.$val->key) ?>">Isi Formulir</a>

                                            <?php } elseif ($val->valid == 3) {
                                                echo "-";
                                            } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        

        <div class="col-md-12" id="berkas">
            <div class="panel panel-warning collapsed">
                <div class="panel-heading">
                    Kelengkapan Berkas
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Booking ID</th>
                                <th>Program Studi</th>
                                <th>Status</th>
                                <th style="text-align: center;" width="40">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $eh = count($load);
                            if ($eh < 1) { ?>
                                <tr>
                                    <td colspan="4"><b>Belum ada data.</b></td>
                                </tr>     
                            <?php } else { ?>
                                <?php foreach ($load as $vals) { ?>
                                <tr>
                                    <td><?php echo $vals->key; ?></td>
                                    <td><?php echo get_prodi($vals->prodi); ?></td>
                                    <td><?php echo completeFile($vals->key,$vals->program,$vals->jenisreg,'L','F'); ?></td>
                                    <td style="text-align: center">
                                        <?= completeFile($vals->key,$vals->program,$vals->jenisreg,'R','F'); ?>
                                    </td>
                                    <!-- <td id="inHereLeft">
                                        <script type="text/javascript">
                                            $("#inHereLeft").load('<?php //echo base_url("dashboard/condForStat/"); ?>'+'L');
                                        </script>
                                    </td>
                                    <td style="text-align: center" id="inHereRight">
                                        <script type="text/javascript">
                                            $("#inHereRight").load('<?php//echo base_url("dashboard/condForStat/"); ?>'+'R');
                                        </script>
                                    </td> -->
                                </tr>
                                <?php } ?>
                            <?php  } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- jika peserta sudah melengkapi berkas maka panel kartu ujian akan ditampilkan namun sebaliknya -->
        <?php 
        $loadd = $this->crud_model->getDetail('tbl_file','userid',$sess['userid'])->num_rows(); 
        if ($loadd > 0) { ?>
        <?php $cekah = $this->crud_model->getDetail('tbl_form_pmb','user_input',$sess['userid']); ?>
        <div class="col-md-12">
            <div class="panel panel-warning collapsed">
                <div class="panel-heading">
                    Kartu Ujian
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Booking ID</th>
                                <th>Program Studi</th>
                                <th>Status</th>
                                <th style="text-align: center;" width="40">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($eh < 1) { ?>
                                <tr>
                                    <td colspan="4"><b>Belum ada data.</b></td>
                                </tr>     
                            <?php } else { ?>
                                <?php foreach ($load as $vals) { ?>
                                <tr>
                                    <td><?php echo $vals->key; ?></td>
                                    <td><?php echo get_prodi($vals->prodi); ?></td>
                                    <!-- versi awal (PMB 2018) -->
                                    <!-- <td>
                                        <?= completeFile($vals->key,$vals->program,$vals->jenisreg,'L','T'); ?>
                                    </td>
                                    <td style="text-align: center">
                                        <?= completeFile($vals->key,$vals->program,$vals->jenisreg,'R','T'); ?>
                                    </td> -->

                                    <!-- versi 1.0 (PMB 2019) -->
                                    <td><?= hasUploadPhoto($vals->key,7,'L') ?></td>
                                    <td><?= hasUploadPhoto($vals->key,7,'R') ?></td>
                                </tr>
                                <?php } ?>
                            <?php  } ?>
                        </tbody>
                    </table>
                    <!-- <p><?php $this->load->helper('cond_helper'); echo alertForFile($cekah->program,$cekah->jenis_pmb,$sess['userid']); ?></p>
                    <hr>
                    <a href="<?php echo base_url('dashboard/printCard'); ?>"><button class="btn btn-success btn-block"><i class="fa fa-print"></i>&nbsp;&nbsp;Kartu Ujian</button></a> -->
                </div>
            </div>
        </div>
        <?php } ?>

    </div>

    <div class="col-md-4">
        <div class="col-md-12">
            <div class="panel panel-indigo collapsed">
                <div class="panel-heading">
                    Calon Mahasiswa
                </div>
                <div class="panel-body">
                    <?php $no = $this->crud_model->getDetail('tbl_form_pmb','user_input',$sess['userid']); 
                    // var_dump($no->num_rows());exit(); ?>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td><b>Nama</b></td>
                                <td><?php echo getName($sess['userid']); ?></td>
                            </tr>
                            <tr>
                                <td><b>ID Pengguna</b></td>
                                <td><?php echo $sess['userid']; ?></td>
                            </tr>
                            <tr>
                                <td><b>Status Pendaftaran</b></td>
                                <td>
                                    <b>
                                        <?php 
                                            if ($no->num_rows() < 1) {
                                                echo 'Belum mendaftar';
                                            } else {
                                                echo 'Terdaftar';
                                            }
                                        ?>
                                    </b>
                                </td>
                            </tr>   
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <?php foreach ($no->result() as $val) { ?>
            <?php 
            if ($val->status_form != 2) { ?>
                 <div class="col-md-12">
                    <div class="panel panel-success collapsed">
                        <div class="panel-heading">
                            Status Kelulusan
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td><b>Nomor Registrasi</b></td>
                                        <td> <?php echo $val->nomor_registrasi; ?> </td>
                                    </tr>
                                    <tr>
                                        <td><b>Gelombang</b></td>
                                        <td> <?php echo $val->gelombang; ?> </td>
                                    </tr>
                                    <tr>
                                        <td><b>Program Studi</b></td>
                                        <td> <?php echo get_prodi($val->prodi); ?> </td>
                                    </tr>
                                    <tr>
                                        <td><b>Status Kelulusan</b></td>
                                        <td><b> <?php if ($val->status_lulus == 1) { echo "LULUS"; } elseif ($val->status_lulus == 2) { echo "TIDAK LULUS"; } else { echo "-"; } ?> </b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>

    </div>

    
    
</div><!--end row-->


<div class="row">
     <div class="col-md-12">
        <div class="panel panel-info collapsed">
            <div class="panel-heading">
                Informasi Penerimaan Mahasiswa baru
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                <thead>
                <tr>
                    <th>GELOMBANG</th>
                    <th>PENDAFTARAN</th>
                    <th>UJIAN</th>
                    <th>PENGUMUMAN</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><b>Gelombang I</b></td>
                    <td>01 November 2017 - 12 Januari 2018</td>
                    <td>14 Januari 2018</td>
                    <td>18 Januari 2018</td>
                </tr>
                <tr>
                    <td><b>Gelombang II</b></td>
                    <td>15 Januari 2018 - 23 Maret 2018</td>
                    <td>25 Maret 2018</td>
                    <td>29 Maret 2018</td>
                </tr>
                <tr>
                    <td><b>Gelombang III</b></td>
                    <td>26 Maret 2018 - 01 Juni 2018</td>
                    <td>3 Juni 2018</td>
                    <td>7 Juni 2018 </td>
                </tr>
                <tr>
                    <td><b>Gelombang IV</b></td>
                    <td>04 Juni 2018 - 03 Agustus 2018</td>
                    <td>5 Agustus 2018</td>
                    <td>9 Agustus 2018</td>
                </tr>
                <tr>
                    <td><b>Briefing Pekan Pengenalan Kehidupan Kampus Bagi Mahasiswa Baru (PK2MB)</b></td>
                    <td colspan="3">23 Agustus 2018 Pukul 10.00 - 14.00 WIB</td>
                </tr>
                <tr>
                    <td><b>Pekan Pengenalan Kehidupan Kampus Bagi Mahasiswa Baru (PK2MB)</b></td>
                    <td colspan="3">24 - 26 Agustus 2018</td>
                </tr>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div><!--end row-->

<!--modal login start-->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="contMods">
            
        </div>
    </div>
</div>
<!--modal login end-->

<!-- modal switch booking -->
<div class="modal fade" id="voidbooking" tabindex="-1" role="dialog" aria-labelledby="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times-circle"></span></button>
                <h3 class="modal-title" id="myModalLabel">Pembatalan Booking</h3>
            </div>
            <div class="modal-body">
                <div class="modal-form">
                    
                    <form role="form" action="<?php echo base_url(); ?>dashboard/booking_form/voidBook" method="post">
                        <div class="form-group">
                            <select class="form-control" name="optprodi">
                                <option disabled="" selected="">-- Pilih Prodi --</option>
                                <?php foreach ($roll as $rows) { ?>
                                    <option value="<?php echo $rows->key; ?>"><?php echo get_prodi($rows->prodi); ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="clearfix">
                            <button type="submit" class="btn  btn-success pull-left">Batalkan</button>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>