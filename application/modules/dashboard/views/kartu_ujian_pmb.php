<?php

//ob_start();
$pdf = new FPDF("L","mm", "A5");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetMargins(2, 0 ,0);

$pdf->SetFont('Arial','B',15); 

//$pdf->image(site_url().'assets/ubhara.png',30,50,90);
$pdf->image('http://172.16.1.5:802/assets/img/logo.gif',5,2,15);
$pdf->setXY(22,4);
$pdf->Cell(200,5,'UNIVERSITAS BHAYANGKARA JAKARTA RAYA',0,5,'L');
$pdf->Ln(2);
$pdf->SetFont('Arial','',15); 
$pdf->setX(22);
$pdf->Cell(200,5,'PENERIMAAN MAHASISWA BARU',0,5,'L');
$pdf->Ln(2);
$pdf->Cell(205,0,'',1,1,'C');
$pdf->setXY(175,1);
$pdf->SetFont('Arial','',8);
$pdf->Cell(200,5,'Nomor Peserta :',0,5,'L');
$pdf->setXY(170,6);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(35,10,$usr->nomor_registrasi,1,10,'L');
$img = $this->db->query('SELECT path from tbl_file where userid = "'.$usr->user_input.'" and tipe = 7')->row()->path;
// $pdf->image('http://pmb.ubharajaya.ac.id'.$img,15,35,50);
$pdf->image('http://172.16.1.5:802/'.$img,15,35,50);


$pdf->SetLeftMargin(5);
$pdf->setY(22);
$pdf->SetFont('Arial','B',11); 
$pdf->Cell(205,5,'KARTU TANDA PESERTA UJIAN',0,1,'C');
$pdf->setXY(14,34);
$pdf->Cell(53,77,'',1,1,'C');

$pdf->SetFont('Arial','',10); 
$pdf->setXY(80,32);
$pdf->Cell(28,5,'Nama Peserta',0,0,'L');
$pdf->Cell(1,5,' : ',0,0,'C');
$pdf->Cell(1,5,$usr->nama,0,1,'L');
$pdf->setXY(80,37);
$pdf->Cell(28,5,'Asal Sekolah',0,0,'L');
$pdf->Cell(1,5,' : ',0,0,'C');

if ($usr->jenis_pmb == 'KV') {
	$pdf->Cell(1,5,$usr->asal_pts_konversi,0,1,'L');	
} else {
	$pdf->Cell(1,5,$usr->asal_sch_maba,0,1,'L');
}

if ($usr->gelombang == 1) {
	$tgl = '13 Januari 2019';
	$not = '17 Januari 2019';
	$daf = '31 Januari 2019';
} elseif ($usr->gelombang == 2) {
	$tgl = '24 Maret 2019';
	$not = '28 Maret 2019';
	$daf = '12 April 2019';
} elseif ($usr->gelombang == 3) {
	$tgl = '26 Mei 2019';
	$not = '31 Mei 2019';
	$daf = '14 Juni 2019';
} elseif ($usr->gelombang == 4) {
	$tgl = '4 Agustus 2019';
	$not = '8 Agustus 2019';
	$daf = '16 Agustus 2019';
} else {
	$tgl = '10 Agustus 2019';
	$not = '15 Agustus 2019';
	$daf = '16 Agustus 2019';
}

$pdf->setXY(80,42);
$pdf->Cell(28,5,'Tanggal Ujian',0,0,'L');
$pdf->Cell(1,5,' : ',0,0,'C');
$pdf->Cell(1,5,$tgl,0,1,'L');

$pdf->setXY(80,47);
$pdf->Cell(28,5,'Lokasi Ujian',0,0,'L');
$pdf->Cell(1,5,' : ',0,0,'C');
$pdf->Cell(1,5,'Kampus II Universitas Bhayangkara Jakarta Raya Bekasi',0,1,'L');

$pdf->setXY(80,52);
$pdf->Cell(28,5,'Program Studi',0,0,'L');
$pdf->Cell(1,5,' : ',0,0,'C');
$pdf->Cell(1,5,get_prodi($usr->prodi),0,1,'L');
$pdf->ln(7);

// table start here
$pdf->setXY(80,57);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(45,9,'Waktu',1,0,'C');
$pdf->Cell(60,9,'Kegiatan',1,0,'C');
$pdf->Cell(20,9,'Paraf',1,1,'C');

$pdf->SetFont('Arial','',10);
$pdf->setXY(80,66);
$pdf->Cell(45,7,'10.00 - 12.00',1,0,'C');
$pdf->Cell(60,7,'Tes Potensi Akademik (TPA)',1,0,'C');
$pdf->Cell(20,7,'',1,1,'C');

$pdf->SetFont('Arial','',10);
$pdf->setXY(80,73);
$pdf->Cell(45,7,$not,1,0,'C');
$pdf->Cell(60,7,'Hasil Tes',1,0,'C');
$pdf->Cell(20,7,'',1,1,'C',1);

$pdf->SetFont('Arial','',10);
$pdf->setXY(80,80);
$pdf->Cell(45,7,substr($not,0,-5).' - '.$daf,1,0,'C');
$pdf->Cell(60,7,'Validasi BRI',1,0,'C');
$pdf->Cell(20,7,'',1,1,'C');

$pdf->SetFont('Arial','',10);
$pdf->setXY(80,87);
$pdf->Cell(45,7,substr($not,0,-5).' - '.$daf,1,0,'C');
$pdf->Cell(60,7,'Validasi BPAK',1,0,'C');
$pdf->Cell(20,7,'',1,1,'C');

$pdf->SetFont('Arial','',10);
$pdf->setXY(80,94);
$pdf->Cell(45,7,substr($not,0,-5).' - '.$daf,1,0,'C');
$pdf->Cell(60,7,'Registrasi BAA',1,0,'C');
$pdf->Cell(20,7,'',1,1,'C');

if ($usr->almet == '3') {
	$almt = 'XXXL';
} elseif ($usr->almet == '2') {
	$almt = 'XXL';
} elseif ($usr->almet == 'X') {
	$almt = 'XL';
} else {
	$almt = $usr->almet;
}

$pdf->SetFont('Arial','',10);
$pdf->setXY(80,101);
$pdf->Cell(45,7,'',1,0,'C',1);
$pdf->Cell(60,7,'Ukuran Almamater',1,0,'C');
$pdf->Cell(20,7,$almt,1,1,'C');
// table end

$pdf->ln(5);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(100,3,'Cetaklah kartu ujian menggunakan printer berwarna. Ketika ujian, Anda harus membawa: kartu tanda peserta ujian ini,',0,1,'L');
$pdf->Cell(100,3,'perlengkapan tulis (balpoint, pensil 2B, penghapus, rautan). Pakaian yang dikenakan : Celana bahan hitam dan',0,1,'L');
$pdf->Cell(100,3,'kemeja putih (Laki-laki), Rok bahan hitam dan kemeja lengan panjang putih (Perempuan). Hadir 30 menit sebelum tes dimulai.',0,1,'L');
$pdf->Cell(100,3,'Pengumuman tes akan diumumkan di mading kampus atau dapat di cek di ubharajaya.ac.id atau pmb.ubharajaya.ac.id.',0,1,'L');
$pdf->Cell(100,3,'Setiap tahap yang telah diselesaikan, peserta harus meminta paraf petugas bersangkutan sebagai bukti telah menyelesaikan tahap.',0,1,'L');
$pdf->image('http://172.16.1.5:802/QRImage/'.str_replace('.', '', $usr->nomor_registrasi).'.png',170,110,30);

$pdf->setXY(5,112);
$pdf->Cell(164,16,'',1,1,'C');

$pdf->SetFont('Arial','',12);
date_default_timezone_set('Asia/Jakarta'); 

//exit();
$pdf->Output('Kartu_ujian_PMB_UBJ'.date('ymd_his').'.PDF','I');

?>

