<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>e-Registration UBJ</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    
    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css">

    <!-- wizard -->
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/wizard/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/wizard/img/favicon.png" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link href="<?php echo base_url(); ?>assets/wizard/css/material-bootstrap-wizard.css" rel="stylesheet" />
    <!-- <link href="<?php echo base_url(); ?>assets/wizard/css/demo.css" rel="stylesheet" /> -->

    <!-- autocomplete lib -->
    <link href="<?php echo base_url();?>assets/js/jquery-ui/jquery-ui.css" rel="stylesheet">

    <!-- Js -->
    <script src="<?php echo base_url();?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
    
    <script src="<?php echo base_url();?>assets/wizard/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui/js/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>assets/wizard/masking/src/jquery.mask.js"></script>

    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/min/waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>

    <script src="<?php echo base_url();?>assets/js/main.js"></script>

    
    

  </head>
  <body>
    <?php $sess = $this->session->userdata('sess_log_pmb'); ?>
    <!-- Header Start -->
    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
           
          </div>
        </div>
      </div>
    </header><!-- header close -->
        
    <!-- Wrapper Start -->
<!-- autocomplete for bank -->
<script type="text/javascript">

    // autocomplete
    jQuery(document).ready(function($) {

        $('input[name^=kdpos]').autocomplete({

            source: '<?php echo base_url('dashboard/postalcode');?>',

            minLength: 3,

            select: function (evt, ui) {

                this.form.kdpos.value = ui.item.value;

            }

        });

    });

</script>

<script>

    $(document).ready(function(){

        // pilih prodi
        $( "#datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '1970:2005',
            dateFormat: 'dd/mm/yy'
        });

        $('#prog').change(function(){
            $.post('<?php echo base_url()?>home/get_jurusan/'+$(this).val(),{},function(get){
                $('#prod').html(get);
            });
        });
        $("#nik").mask("9999999999999999");
        // $("#kdpos").mask("99999");
        // reset input bekerja
        $("#stsb_n").click(function(){
            $("#stsb_txt").val('');
        });

        // tanggal lahir
        //jQuery(function($) {
        //$("#tg_lhr").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
        //});



        // kewarganegaraan
        $('#kwn').hide();
        $('#wni').click(function () {
            $('#kwn').hide();
        });

        $('#wna').click(function () {
            $('#kwn').show();
        });

        
        // status kerja
        $('#stsb_txt').hide();
        $('#stsb_n').click(function () {
            $('#stsb_txt').hide();
        });

        $('#stsb_y').click(function () {
            $('#stsb_txt').show();
        });

        // for form pasca
        // $('#optmaba2').hide();
        // $('#formpasca').hide();
        // $('#jenis1').hide();
        // $('#jenis2').hide();
        // $('#strata1').click(function () {
        //     $('#formpasca').hide();
        //     $('#optmaba1').show();
        //     $('#optmaba2').hide();
        //     $('#jenis1').show();
        //     $('#jenis2').hide();
        // });
        // $('#strata2').click(function () {
        //     $('#formpasca').show();
        //     $('#optmaba1').hide();
        //     $('#optmaba2').show();
        //     $('#jenis1').hide();
        //     $('#jenis2').show();
        // });

        // program
        // $('#konversi').hide();
        // $('#new0').hide();
        // $('#readmisi').hide();

        // $('#r').click(function () {
        //     $('#konversi').hide();
        //     $('#new0').hide();
        //     $('#readmisi').show();
        // });
        // $('#new').click(function () {
        //     $('#konversi').hide();
        //     $('#new0').show();
        //     $('#readmisi').hide();
        // });
        // $('#k').click(function () {
        //     $('#konversi').show();
        //     $('#new0').hide();
        //     $('#readmisi').hide();
        // });

        // kelengkapan
        $('#spd').hide();
        $('#sak').hide();
        $('#tkr').hide(); 
        $('#baa').hide();
        $('#ketren').hide();

        $('#k').click(function () {
            $('#spd').show();
            $('#sak').show();
            $('#tkr').show();
            $('#skhun').hide();
            $('#skl').hide();
            $('#rpt').hide();
        });

        $('#r').click(function () {
            $('#baa').show();
            $('#tkr').show(); 
            $('#ketren').show();
            $('#ijz').hide();
            $('#skhun').hide();
            $('#skl').hide();
            $('#rpt').hide();
        });

        $('#new').click(function () {
            $('#spd').hide();
            $('#sak').hide();
            $('#tkr').hide();
            $('#baa').hide();
            $('#ketren').hide();
            $('.dda').show();
        });

        // bpjs
        $('#bpjs-yes').hide();
        $('#bpjs-y').click(function () {
            $('#bpjs-yes').show();
        }); 

        $('#bpjs-n').click(function () {
            $('#bpjs-yes').hide();
        }); 

        // ubah jenis untuk s2
        // $('#mabas2').hide();
        // $('#reads2').hide();
        // $('#konvs2').hide();
        // $('#n2').click(function () {
        //     $('#mabas2').show();
        //     $('#reads2').hide();
        //     $('#konvs2').hide();
        // });
        // $('#r2').click(function () {
        //     $('#mabas2').hide();
        //     $('#reads2').show();
        //     $('#konvs2').hide();
        // });
        // $('#k2').click(function () {
        //     $('#mabas2').hide();
        //     $('#reads2').hide();
        //     $('#konvs2').show();
        // }); 

    });
</script>




<div class="col-sm-8 col-sm-offset-2">
    <!--      Wizard container-->
    <div class="wizard-container">
        <div class="card wizard-card" data-color="green" id="wizardProfile">
            <form action="<?php echo base_url(); ?>home/add_form" method="post" onsubmit="simpan.disabled = true; simpan.value='Please wait ..'; return true;">
            <!--You can switch " data-color="purple" "  with one of the next bright colors: "green", "orange", "red", "blue"       -->
                <div class="wizard-header">
                    <h3 class="wizard-title">
                       Mohon Lengkapi Formulir Anda
                    </h3>
                    <small>Informasi ini akan digunakan untuk keperluan universitas terhadap calon pendaftar</small>
                </div>
                <div class="wizard-navigation">
                    <ul>
                        <li><a href="#prodi" data-toggle="tab">Pilihan Program Studi</a></li>
                        <li><a href="#about" data-toggle="tab">Data Pribadi</a></li>
                        <li><a href="#account" data-toggle="tab">Data Orang Tua</a></li>
                        <li><a href="#address" data-toggle="tab">Kelengkapan Data</a></li>
                    </ul>
                </div>

                <div class="tab-content">
                    <!-- pilihan program -->
                    <div class="tab-pane" id="prodi">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">school</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Jenis Program <small>(required)</small></label>
                                        <input name="" style="background: #FFE4C4" type="text" value="<?php echo programType($get->program); ?>" class="form-control" disabled="">
                                        <input name="program" type="hidden" value="<?php echo $get->program; ?>" >
                                        <!-- <select name="program" id="prog" class="form-control">
                                            <option selected="" disabled=""></option>
                                            <option value="1" id="strata1">Strata Satu (S1)</option>
                                            <option value="2" id="strata2">Pasca Sarjana (S2)</option>
                                        </select> -->
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">face</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Jenis Pendaftaran <small>(required)</small></label>
                                        <input name="" type="text" style="background: #FFE4C4" value="<?php echo regType($get->jenisreg); ?>" class="form-control" disabled="">
                                        <input name="jenisdaftar" type="hidden" value="<?php echo $get->jenisreg; ?>">
                                        <!-- <select name="jenisdaftar" class="form-control" id="optmaba1">
                                            <option selected="" disabled=""></option>
                                            <option value="MB" id="new">Mahasiswa Baru</option>
                                            <option value="RM" id="r">Mahasiswa Readmisi</option>
                                            <option value="KV" id="k">Mahasiswa Konversi</option>
                                        </select> -->
                                        <!-- <select name="jenisdaftar" class="form-control" id="optmaba2">
                                            <option selected="" disabled=""></option>
                                            <option value="MB" id="n2">Mahasiswa Baru</option>
                                            <option value="RM" id="r2">Mahasiswa Readmisi</option>
                                            <option value="KV" id="k2">Mahasiswa Konversi</option>
                                        </select> -->
                                    </div>
                                </div>

                                
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">menu</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Program Studi Pilihan I<small>(required)</small></label>
                                        <input name="" type="text" style="background: #FFE4C4" value="<?php echo get_prodi($get->prodi); ?>" class="form-control" disabled="">
                                        <input name="prodi" type="hidden" value="<?php echo $get->prodi; ?>">
                                        <!-- <select name="prodi" id="prod" class="form-control">
                                            <option selected="" disabled=""></option>
                                        </select> -->
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">menu</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Program Studi Pilihan II<small>(required)</small></label>
                                        <input name="" type="text" style="background: #FFE4C4" value="<?php echo get_prodi($get->prodi); ?>" class="form-control" disabled="">
                                        <input name="prodi2" type="hidden" value="<?php echo $get->prodi2; ?>">
                                        <!-- <select name="prodi" id="prod" class="form-control">
                                            <option selected="" disabled=""></option>
                                        </select> -->
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">place</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Lokasi Kampus <small>(required)</small></label>
                                        <input name="" type="text" style="background: #FFE4C4" value="<?php echo getCamp($get->camp); ?>" class="form-control" disabled="">
                                        <input name="lokasikampus" type="hidden" value="<?php echo $get->camp; ?>">
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">watch_later</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Opsi Kelas <small>(required)</small></label>
                                        <?php if ($get->program == '2') { ?>
                                            <input name="" type="text" style="background: #FFE4C4" value="Karyawan (C)" class="form-control" disabled="">
                                            <input name="kelas" type="hidden" value="KY">
                                        <?php } else { ?>
                                            <select name="kelas" class="form-control">
                                                <option selected="" disabled=""></option>
                                                <option value="PG">Pagi (A)</option>
                                                <option value="SR">Sore (B)</option>
                                                <option value="KY">Karyawan (C)</option>
                                            </select>
                                        <?php } ?>
                                    </div>
                                </div>

                            </div>

                            <!-- jenis maba untuk s1 -->
                            <div id="jenis1">
                                <?php 
                                    if ($get->program == '1') {
                                        if ($get->jenisreg == 'MB') {
                                            $this->load->view('s1/mb');
                                        } elseif ($get->jenisreg == 'RM') {
                                            $this->load->view('s1/rm');
                                        } else {
                                            $this->load->view('s1/kv');
                                        }
                                    } else {
                                        if ($get->jenisreg == 'MB') {
                                            $this->load->view('s2/mb2');
                                        } elseif ($get->jenisreg == 'RM') {
                                            $this->load->view('s2/rm2');
                                        } else {
                                            $this->load->view('s2/kv2');
                                        }
                                    }
                                ?>
                                <!-- readmisi -->

                                <!-- readmisi end -->


                                <!-- baru -->
                                
                                <!-- baru end -->


                                <!-- konversi -->
                                
                                <!-- konversi end -->
                            </div>
                            <!-- end jenis maba untuk s1 -->

                            <!-- jenis pendaftaran untuk s2 -->
                            <div id="jenis2">
                                <!-- maba start -->
                                
                                <!-- maba end -->

                                <!-- readmisi start -->
                                
                                <!-- readmisi end -->

                                <!-- konversi start -->
                                
                                <!-- konversi end -->
                            </div>
                            <!-- end jenis pendaftaran untuk s2 -->

                            <!-- form pascasarjana -->
                            <!-- <div id="formpasca">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">credit_card</i>
                                        </span>
                                        <div class="form-group label-floating">
                                          <label class="control-label">NPM strata satu (S1) <small> (required)</small></label>
                                          <input type="text"  name="npmsatu" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-10 col-sm-offset-1">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">today</i>
                                        </span>
                                        <div class="form-group label-floating">
                                          <label class="control-label">Tahun Masuk UBJ <small> (required)</small></label>
                                          <input type="text"  name="tahunmasuks2" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <!-- form pascasarjana end -->
                        </div>
                    </div>

                    <!-- data pribadi -->
                    <div class="tab-pane" id="about">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">credit_card</i>
                                    </span>
                                    <div class="form-group label-floating">
                                      <label class="control-label">NIK <small>Nomor Induk Kependudukan (required)</small></label>
                                      <input name="nik" type="text" id="nik" class="form-control" required="" minlength="16" maxlength="16">
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">perm_identity</i>
                                    </span>
                                    <div class="form-group label-floating">
                                      <label class="control-label">Nama Lengkap <small>Sesuai Ijazah Terakhir (required)</small></label>
                                      <input name="nama" type="text" class="form-control" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">today</i>
                                    </span>
                                    <div class="col-sm-8">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Tempat Lahir</label>
                                            <input type="text" class="form-control" name="tpt_lahir" required="">
                                        </div>
                                    </div>
                                    <div class="col-sm-4" style="margin-top: -5px">
                                        <div class="form-group label">
                                            <label class="control-label">Tanggal Lahir</label>
                                            <input type="text" class="form-control" id="datepicker" readonly name="tgl_lahir" required="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">people</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Jenis Kelamin <small>(required)</small></label>
                                        <input  type="radio" name="jk" id="" value="L" required> Laki - Laki &nbsp;&nbsp;
                                        <input  type="radio" name="jk" id="" value="P" required> Perempuan
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">perm_identity</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Golongan <small>(required)</small></label>
                                        <select name="ket" class="form-control" required="">
                                            <option selected="" disabled=""></option>
                                            <option value="1">Polisi / PNS Polri</option>
                                            <option value="2">Keluarga Polisi</option>
                                            <option value="0">Umum</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">home</i>
                                    </span>
                                    
                                    <!-- <label class="control-label">Alamat <small>(required)</small></label> -->
                                    <!-- <textarea name="alamat" class="form-control" required=""></textarea> -->
                                    <div class="col-sm-2">
                                        <div class="form-group label-floating">
                                            <label class="control-label">RT</label>
                                            <input type="text" class="form-control" name="rt" required="">
                                        </div>
                                    </div>
                                    <div class="col-sm-2" style="">
                                        <div class="form-group label-floating">
                                            <label class="control-label">RW</label>
                                            <input type="text" class="form-control" id="" name="rw" required="">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Jalan</label>
                                            <input type="text" class="form-control" name="jalan" required="">
                                        </div>
                                    </div>
                                    <div class="col-sm-4" style="">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Perumahan</label>
                                            <input type="text" class="form-control" id="" name="perum" required="">
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons"></i>
                                    </span>
                                    <div class="col-sm-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Kelurahan</label>
                                            <input type="text" class="form-control" name="lurah" required="">
                                        </div>
                                    </div>
                                    <div class="col-sm-6" style="">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Kecamatan</label>
                                            <input type="text" class="form-control" id="" name="camat" required="">
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">mail</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Kode Pos <small>(required)</small></label>
                                        <input name="kdpos" id="kdpos" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">pin_drop</i>
                                    </span>
                                    <div class="form-group">
                                        <label class="control-label">Kewarganegaraan <small>(required)</small></label><br>
                                        <input type="radio" name="wn" id="wna" value="WNA" onclick="wna()" required> WNA &nbsp;&nbsp;
                                        <input type="radio" name="wn" id="wni" value="WNI" onclick="wni()" required> WNI
                                        <input id="kwn" name="wn_txt" placeholder="Kewarganegaraan Calon Mahasiswa (jika WNA)" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">brightness_low</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Agama <small>(required)</small></label>
                                        <select name="agama" class="form-control" required="">
                                            <option selected="" disabled=""></option>
                                            <option value="ISL">Islam</option>
                                            <option value="KTL">Katolik</option>
                                            <option value="PRT">Protestan</option>
                                            <option value="BDH">Budha</option>
                                            <option value="HND">Hindu</option>
                                            <option value="OTH">Lainnya</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">wc</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Status Nikah <small>(required)</small></label>
                                        <input type="radio" name="stsm" value="Y" required> Menikah &nbsp;&nbsp;
                                        <input type="radio" name="stsm" value="N" required> Belum Menikah &nbsp;&nbsp;
                                        <input type="radio" name="stsm" value="D" required> Janda/Duda
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">domain</i>
                                    </span>
                                    <div class="form-group">
                                        <label class="control-label">Bekerja <small>(required)</small></label><br>
                                        <input type="radio" name="stsb" id="stsb_n" value="N" required> Belum Bekerja &nbsp;&nbsp;
                                        <input type="radio" name="stsb" id="stsb_y" value="Y"> Bekerja &nbsp;&nbsp; 
                                        <input type="text" class="form-control span3" id="stsb_txt"  name="stsb_txt" placeholder="Tempat Bekerja Calon Mahasiswa">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">phone_iphone</i>
                                    </span>
                                    <div class="form-group label-floating">
                                      <label class="control-label">Telepon / HP <small>nomor aktif (required)</small></label>
                                      <input type="number"  name="tlp" class="form-control" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">phone_in_talk</i>
                                    </span>
                                    <div class="form-group label-floating">
                                      <label class="control-label">No. Telpon / HP Wali <small>nomor aktif (required)</small></label>
                                      <input type="number"  name="tlp2" class="form-control" required="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- data orang tua -->
                    <div class="tab-pane" id="account">
                        <div class="row">
                            <!-- detail ayah -->
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nama Ayah <small>(required)</small></label>
                                        <input class="form-control" type="text" name="nm_ayah" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">school</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pendidikan Ayah <small>(required)</small></label>
                                        <select name="didik_ayah" id="" class="form-control" required="">
                                            <option selected="" disabled=""></option>
                                            <option value="NSD">Tidak tamat SD</option>
                                            <option value="YSD">Tamat SD</option>
                                            <option value="SMP">Tamat SLTP</option>
                                            <option value="SMA">Tamat SLTA</option>
                                            <option value="DPL">Diploma</option>
                                            <option value="SMD">Sarjana Muda</option>
                                            <option value="SRJ">Sarjana</option>
                                            <option value="PSC">Pascasarjana</option>
                                            <option value="DTR">Doctor</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">work</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pekerjaan Ayah <small>(required)</small></label>
                                        <select name="workdad" id="" class="form-control" required="">
                                            <option selected="" disabled=""></option>
                                            <option value="PN">Pegawai Negeri</option>
                                            <option value="TP">TNI / POLRI</option>
                                            <option value="PS">Pegawai Swasta</option>
                                            <option value="WU">Wirausaha</option>
                                            <option value="PE">Pensiun</option>
                                            <option value="TK">Tidak Bekerja</option>
                                            <option value="LL">Lain-lain</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">timeline</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Status Hidup Ayah <small>(required)</small></label>
                                        <select name="life_statdad" id="" class="form-control" required="">
                                            <option selected="" disabled=""></option>
                                            <option value="MH">Masih Hidup</option>
                                        <option value="SM">Sudah Meninggal</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- detail ayah /end -->
                            <hr>
                            <!-- detail ibu -->
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nama Ibu <small>(required)</small></label>
                                        <input class="form-control" type="text" name="nm_ibu" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">school</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pendidikan Ibu <small>(required)</small></label>
                                        <select name="didik_ibu" id="" class="form-control" required="">
                                            <option selected="" disabled=""></option>
                                            <option value="NSD">Tidak tamat SD</option>
                                            <option value="YSD">Tamat SD</option>
                                            <option value="SMP">Tamat SLTP</option>
                                            <option value="SMA">Tamat SLTA</option>
                                            <option value="DPL">Diploma</option>
                                            <option value="SMD">Sarjana Muda</option>
                                            <option value="SRJ">Sarjana</option>
                                            <option value="PSC">Pascasarjana</option>
                                            <option value="DTR">Doctor</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">work</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pekerjaan Ibu <small>(required)</small></label>
                                        <select name="workmom" id="" class="form-control" required="">
                                            <option selected="" disabled=""></option>
                                            <option value="PN">Pegawai Negeri</option>
                                            <option value="TP">TNI / POLRI</option>
                                            <option value="PS">Pegawai Swasta</option>
                                            <option value="WU">Wirausaha</option>
                                            <option value="PE">Pensiun</option>
                                            <option value="TK">Tidak Bekerja</option>
                                            <option value="LL">Lain-lain</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">timeline</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Status Hidup Ibu <small>(required)</small></label>
                                        <select name="life_statmom" id="" class="form-control" required="">
                                            <option selected="" disabled=""></option>
                                            <option value="MH">Masih Hidup</option>
                                        <option value="SM">Sudah Meninggal</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">attach_money</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Jumlah Penghasilan Orang Tua <small>(required)</small></label>
                                        <input type="radio" value="1" name="gaji" required> Rp 1,000,000 - 2,000,000 &nbsp;&nbsp;
                                    
                                        <input type="radio" value="2" name="gaji"> Rp 2,100,000 - 4,000,000 <br>
                                    
                                        <input type="radio" value="3" name="gaji"> Rp 4,100,000 - 5,999,000  &nbsp;&nbsp;
                                
                                        <input type="radio" value="4" name="gaji"> >= Rp 6,000,000
                                    </div>
                                </div>
                            </div>
                            <!-- detail ibu /end -->
                        </div>
                    </div>

                    <!-- kelengkapan data -->
                    <div class="tab-pane" id="address">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">credit_card</i>
                                    </span>
                                    <div class="form-group">
                                        <label class="control-label">Pengguna BPJS <small>(required)</small></label><br>
                                        <input type="radio" id="bpjs-y" name="bpjs" value="y" required> Ya &nbsp;&nbsp;
                                        <input type="radio" id="bpjs-n" name="bpjs" value="n" > Tidak &nbsp;&nbsp; 
                                        <input class="form-control" id="bpjs-yes" type="text" placeholder="Nomor BPJS" name="nobpjs">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">motorcycle</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Alat Transportasi <small>(required)</small></label>
                                        <select name="transportasi" id="" class="form-control" required="">
                                            <option selected="" disabled=""></option>
                                            <option value="MBL">Mobil</option>
                                            <option value="MTR">Motor</option>
                                            <option value="AKT">Angkutan Umum</option>
                                            <option value="SPD">Sepeda</option>
                                            <option value="JKK">Jalan Kaki</option>
                                            <option value="ADG">Andong</option>
                                            <option value="KRT">Kereta</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">question_answer</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Referensi Memilih UBJ <small>(required)</small></label>
                                        <input class="form-control" id="" type="text" name="refer" required="">
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" value="<?php echo $key ?>" name="kunci">

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Ukuran Almamater <small>(required)</small></label>
                                        <select name="sizealmet" id="" class="form-control" required="">
                                            <option selected="" disabled=""></option>
                                            <option value="S">S</option>
                                            <option value="M">M</option>
                                            <option value="L">L</option>
                                            <option value="X">XL</option>
                                            <option value="2">XXL</option>
                                            <option value="3">XXXL</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">assignment</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Surat Kelengkapan <small>(required)</small></label>
                                        <div class="dda" id="akt"><input type="checkbox" name="lengkap[]" value="AKT"> Akte Kelahiran &nbsp;&nbsp;</div>
                                        <div class="dda" id="kk"><input type="checkbox" name="lengkap[]" value="KK"> Kartu Keluarga (KK) &nbsp;&nbsp; </div>
                                        <div class="dda" id="ktp"><input type="checkbox" name="lengkap[]" value="KTP"> Kartu Tanda Penduduk (KTP)  &nbsp;&nbsp;</div>
                                        <div class="dda" id="rpt"><input type="checkbox" name="lengkap[]" value="RP"> Rapot  &nbsp;&nbsp;</div>
                                        <div class="dda" id="skhun"><input type="checkbox" name="lengkap[]" value="SKHUN"> SKHUN  </div>
                                        <div class="dda" id="foto"><input type="checkbox" name="lengkap[]" value="FT"> Foto (3x4 dan 4x6) &nbsp;&nbsp;</div>
                                        <div class="dda" id="ijz"><input type="checkbox" name="lengkap[]" value="IJZ"> Ijazah &nbsp;&nbsp; </div>
                                        <div class="dda" id="skl"><input type="checkbox" name="lengkap[]" value="SKL"> Surat Kelulusan  &nbsp;&nbsp;<br></div>
                                        
                                        <div id="spd"><input type="checkbox" name="lengkap[]" value="SPD"> Surat Pindah &nbsp;&nbsp;</div>
                                        <div id="sak"><input type="checkbox" name="lengkap[]" value="SAK"> Sertifikat Akreditasi &nbsp;&nbsp;</div>

                                        <div id="tkr"><input type="checkbox" name="lengkap[]" value="TKR"> Transkrip  &nbsp;&nbsp;</div>
                                        <div id="baa"><input type="checkbox" name="lengkap[]" value="LBAA"> Laporan BAA  &nbsp;&nbsp;</div>
                                        <div id="ketren"><input type="checkbox" name="lengkap[]" value="KTREN"> Keterangan RENKEU </div>

                                        <div class="dda" id="lkp"><input type="checkbox" name="lengkap[]" value="LLKP"> Lengkap Administratif  &nbsp;&nbsp;</div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
                <!-- parameter untuk $boo -->
                <input type="hidden" name="sesi" value="<?php echo $prodi; ?>">
                <div class="wizard-footer">
                    <div class="pull-right">
                        <input type='button' class='btn btn-next btn-fill btn-success btn-wd' name='next' value='Next' />
                        <input type='submit' id="simpan" class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='Finish' />
                    </div>

                    <div class="pull-left">
                        <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' />
                    </div>
                    <div class="clearfix"></div>
                </div>

            </form>
        </div>
    </div> <!-- wizard container -->
    <br><br>
</div>  
    
    <script>
        jQuery(function($){
           $("#tg_lhr").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
        });
    </script>

    <!-- mask -->
    <!-- <script src="<?php //echo base_url('assets/wizard/dist/jQuery-mask/jquery.mask.js'); ?>"></script> -->
    <!-- <script src="<?php //echo base_url('assets/wizard/dist/jQuery-mask/jquery.mask.min.js'); ?>"></script> -->

    <!-- wizard -->
    
    <script src="<?php echo base_url(); ?>assets/wizard/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/wizard/js/jquery.bootstrap.js" type="text/javascript"></script>
    <!--  Plugin for the Wizard -->
    <script src="<?php echo base_url(); ?>assets/wizard/js/material-bootstrap-wizard.js"></script>
    <!--  More information about jquery.validate here: http://jqueryvalidation.org/  -->
    <script src="<?php echo base_url(); ?>assets/wizard/js/jquery.validate.min.js"></script>

            
            
  <!-- <script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script> -->
            
    
    </body>
</html>