<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>e-Registration UBJ</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    
    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css">
    <link href="<?php echo base_url();?>assets/js/jquery-ui/jquery-ui.css" rel="stylesheet">

    <!-- wizard -->
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/wizard/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/wizard/img/favicon.png" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link href="<?php echo base_url(); ?>assets/wizard/css/material-bootstrap-wizard.css" rel="stylesheet" />
    <!-- <link href="<?php echo base_url(); ?>assets/wizard/css/demo.css" rel="stylesheet" /> -->
    <!-- Js -->
    <script src="<?php echo base_url();?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->


    
    <script src="<?php echo base_url();?>assets/wizard/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui/js/jquery-ui.js"></script>
    
    <script src="<?php echo base_url();?>assets/wizard/masking/dist/jquery.mask.min.js"></script>

    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/min/waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>

    <script src="<?php echo base_url();?>assets/js/main.js"></script>

    
    

  </head>
  <body>
    <!-- Header Start -->
    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
           
          </div>
        </div>
      </div>
    </header><!-- header close -->
        
    <!-- Wrapper Start -->
    <script type="text/javascript">

    // autocomplete
    jQuery(document).ready(function($) {

        $('input[name^=kdpos]').autocomplete({

            source: '<?php echo base_url('dashboard/postalcode');?>',

            minLength: 3,

            select: function (evt, ui) {

                this.form.kdpos.value = ui.item.value;

            }

        });

    });

</script>
            
<script>

    $(document).ready(function(){

        // pilih prodi
        $('#prog').change(function(){
            $.post('<?php echo base_url()?>home/get_jurusan/'+$(this).val(),{},function(get){
                $('#prod').html(get);
            });
        });

        // kewarganegaraan
        $('#kwn').hide();
        $('#wni').click(function () {
            $('#kwn').hide();
        });

        $('#wna').click(function () {
            $('#kwn').show();
        });

        $( "#datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '1970:2005',
            dateFormat: 'dd/mm/yy'
        });

        // status kerja
        $('#stsb_txt').hide();
        $('#stsb_n').click(function () {
            $('#stsb_txt').hide();
        });

        $('#stsb_y').click(function () {
            $('#stsb_txt').show();
        });
        

        $('#new').click(function () {
            $('#spd').hide();
            $('#sak').hide();
            $('#tkr').hide();
            $('#baa').hide();
            $('#ketren').hide();
            $('.dda').show();
        });

        // bpjs
        $('#bpjs-yes').hide();
        $('#bpjs-y').click(function () {
            $('#bpjs-yes').show();
        }); 

        $('#bpjs-n').click(function () {
            $('#bpjs-yes').hide();
            $('#bpjs-yes').val('');
        }); 

         

    });
</script>
<div class="col-sm-8 col-sm-offset-2">
    <!--      Wizard container        -->
    <div class="wizard-container">
        <div class="card wizard-card" data-color="red" id="wizardProfile">
            <form action="<?php echo base_url('dashboard/saveFromEdit/'); ?>" method="post"  onsubmit="simpan. = true; simpan.value='Please wait ..'; return true;">
            <!--You can switch " data-color="purple" "  with one of the next bright colors: "green", "orange", "red", "blue"       -->
                <div class="wizard-header">
                    <h3 class="wizard-title">
                       Ubah Formulir Anda
                    </h3>
                    <small>Anda dapat merubah formulir pendaftaran anda.</small>
                </div>
                <div class="wizard-navigation">
                    <ul>
                        <li><a href="#prodi" data-toggle="tab">Pilihan Program Studi</a></li>
                        <li><a href="#about" data-toggle="tab">Data Pribadi</a></li>
                        <li><a href="#account" data-toggle="tab">Data Orang Tua</a></li>
                        <li><a href="#address" data-toggle="tab">Kelengkapan Data</a></li>
                    </ul>
                </div>

                <div class="tab-content">
                    <!-- pilihan program -->
                    <div class="tab-pane" id="prodi">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">school</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Jenis Program <small>(required)</small></label>
                                            <?php 
                                                if ($detl['program'] == 1) { 
                                                    $jenisprogram = "Strata Satu (S1)";
                                                } else {
                                                    $jenisprogram = "Pasca Sarjana (S2)";
                                                } 
                                            ?>
                                            <input id="strata1" value="<?php echo $jenisprogram; ?>" readonly="" style="background-color: #F0E68C" class="form-control">
                                            <input type="hidden" value="<?php echo $detl['program']; ?>" name="program">
                                        </select>
                                    </div>
                                </div>

                                <input type="hidden" value="<?php echo $detl['nomor_registrasi']; ?>" name="noreg">

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">face</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Jenis Pendaftaran <small>(required)</small></label>
                                        <?php 
                                            if ($detl['jenis_pmb'] == 'MB') { 
                                                $jenisdaftar = "Mahasiswa Baru";
                                            } elseif ($detl['jenis_pmb'] == 'RM') {
                                                $jenisdaftar = "Readmisi";
                                            } else {
                                                $jenisdaftar = "Konversi";
                                            }
                                        ?>
                                        <input id="" value="<?php echo $jenisdaftar; ?>" readonly="" style="background-color: #F0E68C" class="form-control">
                                        <input type="hidden" name="jenisdaftar" value="<?php echo $detl['jenis_pmb']; ?>">
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">watch_later</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Opsi Kelas <small>(required)</small></label>
                                        <?php 
                                            if ($detl['kelas'] == 'PG') { 
                                                $kelas = "Pagi (A)";
                                            } elseif ($detl['kelas'] == 'SR') {
                                                $kelas = "Sore (B)";
                                            } else {
                                                $kelas = "Karyawan (C)";
                                            }
                                        ?>
                                        <input id="" value="<?php echo $kelas; ?>" readonly="" style="background-color: #F0E68C" class="form-control">
                                        <input type="hidden" name="kelas" value="<?php echo $detl['kelas']; ?>">
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">place</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Lokasi Kampus <small>(required)</small></label>
                                        <?php 
                                            if ($detl['kampus'] == 'bks') { 
                                                $kampus = "Bekasi";
                                            } else {
                                                $kampus = "Jakarta";
                                            }
                                        ?>
                                        <input id="" value="<?php echo $kampus; ?>" readonly="" style="background-color: #F0E68C" class="form-control">
                                        <input type="hidden" value="<?php echo $detl['kampus']; ?>" name="lokasikampus">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">menu</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Program Studi <small>(required)</small></label>
                                        <input id="" value="<?php echo get_prodi($detl['prodi']); ?>" readonly="" style="background-color: #F0E68C" class="form-control">
                                        <input type="hidden" value="<?php echo $detl['prodi']; ?>" name="prodi">
                                    </div>
                                </div>
                            </div>

                            <!-- jenis maba untuk s1 -->
                            <?php if($detl['program'] == 1) { ?>
                            <div id="jenis1">
                                <!-- readmisi -->
                                <?php if($detl['jenis_pmb'] == 'RM') { ?>
                                <div id="readmisi">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">credit_card</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">NPM <small>(required)</small></label>
                                                <input name="npm_readmisi" value="<?php echo $detl['npm_lama_readmisi']; ?>" type="text" class="form-control" />
                                                <input type="hidden" value="<?php echo $detl['npm_lama_readmisi']; ?>" name="npm_readmisi">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">today</i>
                                            </span>
                                            <div class="col-sm-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Tahun Masuk UBJ</label>
                                                    <input type="text" class="form-control" value="<?php echo $detl['tahun_masuk_readmisi']; ?>" name="thmasuk_readmisi" />
                                                    <input type="hidden" value="<?php echo $detl['tahun_masuk_readmisi']; ?>" name="thmasuk_readmisi">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Sampai Dengan Semester</label>
                                                    <input type="text" class="form-control" id="" value="<?php echo $detl['smtr_keluar_readmisi']; ?>" name="smtr_readmisi" />
                                                    <input type="hidden" value="<?php echo $detl['smtr_keluar_readmisi']; ?>" name="smtr_readmisi">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php } elseif($detl['jenis_pmb'] == 'MB') { ?>
                                <!-- baru -->
                                <div id="new0">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">local_library</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Asal Sekolah <small>(required)</small></label>
                                                <input value="<?php echo $detl['asal_sch_maba']; ?>" type="text" name="asal_sch" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">credit_card</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">NISN <small>(required)</small></label>
                                                <input value="<?php echo $detl['nisn']; ?>" type="text" name="nisn" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">pin_drop</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Kota Asal Sekolah <small>(required)</small></label>
                                                <input value="<?php echo $detl['kota_sch_maba']; ?>" type="text" name="kota_sch" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">map</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Kelurahan Asal Sekolah <small>(required)</small></label>
                                                <input value="<?php echo $detl['daerah_sch_maba']; ?>" type="text" name="daerah_sch" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">domain</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Kategori Sekolah <small>(required)</small></label>
                                                <input type="radio" name="jenis_skl" value="NGR" <?php if($detl['kategori_skl'] == 'NGR') { echo "checked='checked'"; } ?> > NEGERI &nbsp;&nbsp;
                                                <input type="radio" name="jenis_skl" value="SWT" <?php if($detl['kategori_skl'] == 'SWT') { echo "checked='checked'"; } ?> > SWASTA &nbsp;&nbsp; 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">school</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Jenis Sekolah <small>(required)</small></label>
                                                <input type="radio" name="jenis_sch_maba" value="SMA" <?php if($detl['jenis_sch_maba'] == 'SMA') { echo "checked='checked'"; } ?> > SMA &nbsp;&nbsp;
                                                <input type="radio" name="jenis_sch_maba" value="SMK" <?php if($detl['jenis_sch_maba'] == 'SMK') { echo "checked='checked'"; } ?> > SMK &nbsp;&nbsp; 
                                                <input type="radio" name="jenis_sch_maba" value="MDA" <?php if($detl['jenis_sch_maba'] == 'MDA') { echo "checked='checked'"; } ?> > MA  &nbsp;&nbsp;
                                                <input type="radio" name="jenis_sch_maba" value="SMB" <?php if($detl['jenis_sch_maba'] == 'SMB') { echo "checked='checked'"; } ?> > SMTB  &nbsp;&nbsp;
                                                <input type="radio" name="jenis_sch_maba" value="OTH" <?php if($detl['jenis_sch_maba'] == 'OTH') { echo "checked='checked'"; } ?> > Lainnya
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">near_me</i>
                                            </span>
                                            <div class="col-sm-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Jurusan</label>
                                                    <input type="text" class="form-control" name="jur_sch" value="<?php echo strtoupper($detl['jur_maba']); ?>" />
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Tahun Lulus</label>
                                                    <input type="text" class="form-control" name="lulus_sch" value="<?php echo $detl['lulus_maba']; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php } elseif($detl['jenis_pmb'] == 'KV') { ?>
                                <!-- konversi -->
                                <div id="konversi">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">domain</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Nama Perguruan Tinggi <small>(required)</small></label>
                                                <input name="asal_pts" type="text" class="form-control" value="<?php echo strtoupper($detl['asal_pts_konversi']); ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">map</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Kota PTN/PTS <small>(required)</small></label>
                                                <input name="kota_pts" type="text" class="form-control" value="<?php echo strtoupper($detl['kota_pts_konversi']); ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">local_library</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Program Studi <small>(required)</small></label>
                                                <input name="prodi_pts" type="text" class="form-control" value="<?php echo strtoupper($detl['prodi_pts_konversi']); ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">today</i>
                                            </span>
                                            <div class="col-sm-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Tahun Lulus</label>
                                                    <input type="text" class="form-control" name="lulus_pts" value="<?php echo $detl['tahun_lulus_konversi']; ?>" />
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Semester</label>
                                                    <input type="text" class="form-control" id="" name="smtr_pts" value="<?php echo $detl['smtr_lulus_konversi']; ?>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">credit_card</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">NPM/NIM Asal <small>(required)</small></label>
                                                <input name="npm_pts" type="text" class="form-control" value="<?php echo $detl['npm_pts_konversi']; ?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- konversi end -->
                                <?php } ?>
                            </div>
                            <!-- end jenis maba untuk s1 -->

                            <?php } else { ?>
                            <!-- jenis pendaftaran untuk s2 -->
                            <div id="jenis2">
                                <?php if($detl['jenis_pmb'] == 'MB') { ?>
                                <!-- maba start -->
                                <div id="mabas2">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">domain</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Asal Universitas</label>
                                                <input class="form-control span4" type="text" name="asal_sch" value="<?php echo $detl['asal_sch_maba']; ?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- maba end -->

                                <?php } elseif($detl['jenis_pmb'] == 'RM') { ?>
                                <!-- readmisi start -->
                                <div id="reads2">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">credit_card</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">NPM Lama</label>
                                                <input class="form-control span4" type="text" name="npmsatu" value="<?php echo $detl['npm_lama_s2']; ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">today</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Tahun Masuk di UBJ</label>
                                                <input class="form-control span4" type="text" name="tahunmasuks2" value="<?php echo $detl['th_masuk_s2']; ?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- readmisi end -->

                                <?php } elseif($detl['jenis_pmb'] == 'KV') { ?>
                                <!-- konversi start -->
                                <div id="konvs2">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">domain</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Jenis Perguruan Tinggi</label>
                                                <input type="radio" name="jenis_skl" value="NGR" <?php if($detl['kategori_skl'] == 'NGR') { echo "checked='checked'"; } ?> > NEGERI &nbsp;&nbsp;&nbsp;
                                                <input type="radio" name="jenis_skl" value="SWT" <?php if($detl['kategori_skl'] == 'SWT') { echo "checked='checked'"; } ?> > SWASTA
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">domain</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Nama Perguruan Tinggi</label>
                                                <input class="form-control span4" type="text" name="asal_pts" value="<?php echo strtoupper($detl['asal_pts_konversi']); ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">local_library</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Program Studi</label>
                                                <input class="form-control span4" type="text" name="prodi_pts" value="<?php echo strtoupper($detl['prodi_pts_konversi']); ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">today</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Tahun lulus/Semester</label>
                                                <input class="form-control span4" type="text" name="lulus_pts" value="<?php echo $detl['tahun_lulus_konversi']; ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">map</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Kota Asal PTS/PTN</label>
                                                <input class="form-control span4" type="text" name="kota_pts" value="<?php echo strtoupper($detl['kota_pts_konversi']); ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">credit_card</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">NPM/NIM</label>
                                                <input class="form-control span4" type="text" name="npm_pts" value="<?php echo strtoupper($detl['npm_pts_konversi']); ?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- konversi end -->
                                <?php } ?>
                            </div>
                            <!-- end jenis pendaftaran untuk s2 -->
                            <?php } ?>

                            <!-- form pascasarjana -->
                            <!-- <div id="formpasca">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">credit_card</i>
                                        </span>
                                        <div class="form-group label-floating">
                                          <label class="control-label">NPM strata satu (S1) <small> (required)</small></label>
                                          <input type="text"  name="npmsatu" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-10 col-sm-offset-1">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">today</i>
                                        </span>
                                        <div class="form-group label-floating">
                                          <label class="control-label">Tahun Masuk UBJ <small> (required)</small></label>
                                          <input type="text"  name="tahunmasuks2" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <!-- form pascasarjana end -->
                        </div>
                    </div>

                    <!-- data pribadi -->
                    <div class="tab-pane" id="about">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">credit_card</i>
                                    </span>
                                    <div class="form-group label-floating">
                                      <label class="control-label">NIK <small>Nomor Induk Kependudukan (min. 16 karakter)(required)</small></label>
                                      <input name="nik" type="text" class="form-control" value="<?php echo $detl['nik']; ?>" maxlength="16" minlength="16" />
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">perm_identity</i>
                                    </span>
                                    <div class="form-group label-floating">
                                      <label class="control-label">Nama Lengkap <small>Sesuai Ijazah Terakhir (required)</small></label>
                                      <input name="nama" type="text" class="form-control" value="<?php echo $detl['nama']; ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">today</i>
                                    </span>
                                    <div class="col-sm-8">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Tempat Lahir</label>
                                            <input type="text" class="form-control" name="tpt_lahir" value="<?php echo $detl['tempat_lahir']; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Tanggal Lahir</label>
                                            <input type="text" class="form-control" id="datepicker" readonly name="tgl_lahir" value="<?php echo $detl['tgl_lahir']; ?>" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">people</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <input  type="radio" name="jk" id="" value="L" <?php if ($detl['kelamin'] == 'L') { echo 'checked="checked"'; } ?> required> Laki - Laki &nbsp;&nbsp;
                                        <input  type="radio" name="jk" id="" value="P" <?php if ($detl['kelamin'] == 'P') { echo 'checked="checked"'; } ?> required> Perempuan
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">home</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Alamat <small>(required)</small></label>
                                        <?php $add = explode(',', $detl['alamat']); ?>
                                        <div class="col-sm-2">
                                            <div class="form-group label-floating">
                                                <label class="control-label">RT</label>
                                                <input type="text" class="form-control" name="rt" value="<?php echo $add[0]; ?>" required="">
                                            </div>
                                        </div>
                                        <div class="col-sm-2" style="">
                                            <div class="form-group label-floating">
                                                <label class="control-label">RW</label>
                                                <input type="text" class="form-control" id="" name="rw" value="<?php echo $add[1]; ?>" required="">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Kelurahan</label>
                                                <input type="text" class="form-control" name="lurah" value="<?php echo $add[2]; ?>" required="">
                                            </div>
                                        </div>
                                        <div class="col-sm-4" style="">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Kecamatan</label>
                                                <input type="text" class="form-control" id="" name="camat" value="<?php echo $add[3]; ?>" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">mail</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Kode Pos (Ketik nama daerah)<small>(required)</small></label>
                                        <input name="kdpos" type="text" class="form-control" value="<?php echo $detl['kd_pos']; ?>" />
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">pin_drop</i>
                                    </span>
                                    <div class="form-group">
                                        <label class="control-label">Kewarganegaraan <small>(required)</small></label><br>
                                        
                                        <input type="radio" name="wn" id="wna" <?php if ($detl['status_wn'] == 'WNA') { echo 'checked="checked"'; } ?> value="WNA" onclick="wna()" required> WNA &nbsp;&nbsp;
                                        <input type="radio" name="wn" id="wni" <?php if ($detl['status_wn'] == 'WNI') { echo 'checked="checked"'; } ?> value="WNI" onclick="wni()" required> WNI
                                        <input id="kwn" name="wn_txt" placeholder="Kewarganegaraan Calon Mahasiswa (jika WNA)" value="<?php echo $detl['kewarganegaraan']; ?>" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">brightness_low</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Agama <small>(required)</small></label>
                                        <select name="agama" class="form-control" >
                                            <option value="ISL" <?php if($detl['agama'] == 'ISL') { echo "checked='checked'"; } ?> >Islam</option>
                                            <option value="KTL" <?php if($detl['agama'] == 'KTL') { echo "checked='checked'"; } ?> >Katolik</option>
                                            <option value="PRT" <?php if($detl['agama'] == 'PRT') { echo "checked='checked'"; } ?> >Protestan</option>
                                            <option value="BDH" <?php if($detl['agama'] == 'BDH') { echo "checked='checked'"; } ?> >Budha</option>
                                            <option value="HND" <?php if($detl['agama'] == 'HND') { echo "checked='checked'"; } ?> >Hindu</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">wc</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Status Nikah <small>(required)</small></label>
                                        <input type="radio" name="stsm" <?php if ($detl['status_nikah'] == 'Y') { echo 'checked="checked"'; } ?> value="Y" required> Menikah &nbsp;&nbsp;
                                        <input type="radio" name="stsm" <?php if ($detl['status_nikah'] == 'N') { echo 'checked="checked"'; } ?> value="N" required> Belum Menikah &nbsp;&nbsp;
                                        <input type="radio" name="stsm" <?php if ($detl['status_nikah'] == 'D') { echo 'checked="checked"'; } ?> value="D" required> Janda/Duda
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">domain</i>
                                    </span>
                                    <div class="form-group">
                                        <label class="control-label">Bekerja <small>(required)</small></label><br>
                                        <input type="radio" name="stsb" id="stsb_n" value="N" <?php if ($detl['status_kerja'] == 'N') { echo 'checked="checked"'; } ?> required> Belum Bekerja &nbsp;&nbsp;
                                        <input type="radio" name="stsb" id="stsb_y" value="Y" <?php if ($detl['status_kerja'] == 'Y') { echo 'checked="checked"'; } ?>> Bekerja &nbsp;&nbsp; 
                                        <input type="text" class="form-control span3" id="stsb_txt" value="<?php echo $detl['pekerjaan']; ?>" name="stsb_txt" placeholder="Tempat Bekerja Calon Mahasiswa">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">phone_iphone</i>
                                    </span>
                                    <div class="form-group label-floating">
                                      <label class="control-label">Telepon / HP <small>nomor aktif (required)</small></label>
                                      <input type="text"  name="tlp" type="text" class="form-control" value="<?php echo $detl['tlp']; ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">phone_in_talk</i>
                                    </span>
                                    <div class="form-group label-floating">
                                      <label class="control-label">No. Telpon / HP Wali <small>nomor aktif (required)</small></label>
                                      <input type="text"  name="tlp2" type="text" class="form-control" value="<?php echo $detl['tlp2']; ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- data orang tua -->
                    <div class="tab-pane" id="account">
                        <div class="row">
                            <!-- detail ayah -->
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nama Ayah <small>(required)</small></label>
                                        <input class="form-control" type="text" name="nm_ayah"  value="<?php echo strtoupper($detl['nm_ayah']); ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">school</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pendidikan Ayah <small>(required)</small></label>
                                        <select name="didik_ayah" id="" class="form-control">
                                            <option value="NSD" <?php if($detl['didik_ayah'] == 'NSD') { echo "selected=''"; } ?>>Tidak tamat SD</option>
                                            <option value="YSD" <?php if($detl['didik_ayah'] == 'YSD') { echo "selected=''"; } ?>>Tamat SD</option>
                                            <option value="SMP" <?php if($detl['didik_ayah'] == 'SMP') { echo "selected=''"; } ?>>Tamat SLTP</option>
                                            <option value="SMA" <?php if($detl['didik_ayah'] == 'SMA') { echo "selected=''"; } ?>>Tamat SLTA</option>
                                            <option value="DPL" <?php if($detl['didik_ayah'] == 'DPL') { echo "selected=''"; } ?>>Diploma</option>
                                            <option value="SMD" <?php if($detl['didik_ayah'] == 'SMD') { echo "selected=''"; } ?>>Sarjana Muda</option>
                                            <option value="SRJ" <?php if($detl['didik_ayah'] == 'SRJ') { echo "selected=''"; } ?>>Sarjana</option>
                                            <option value="PSC" <?php if($detl['didik_ayah'] == 'PSC') { echo "selected=''"; } ?>>Pascasarjana</option>
                                            <option value="DTR" <?php if($detl['didik_ayah'] == 'DTR') { echo "selected=''"; } ?>>Doctor</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">work</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pekerjaan Ayah <small>(required)</small></label>
                                        <select name="workdad" id="" class="form-control">
                                            <option value="PN" <?php if($detl['workdad'] == 'PN') { echo "selected=''"; } ?>>Pegawai Negeri</option>
                                            <option value="TP" <?php if($detl['workdad'] == 'TP') { echo "selected=''"; } ?>>TNI / POLRI</option>
                                            <option value="PS" <?php if($detl['workdad'] == 'PS') { echo "selected=''"; } ?>>Pegawai Swasta</option>
                                            <option value="WU" <?php if($detl['workdad'] == 'WU') { echo "selected=''"; } ?>>Wirausaha</option>
                                            <option value="PS" <?php if($detl['workdad'] == 'PE') { echo "selected=''"; } ?>>Pensiun</option>
                                            <option value="TK" <?php if($detl['workdad'] == 'TK') { echo "selected=''"; } ?>>Tidak Bekerja</option>
                                            <option value="LL" <?php if($detl['workdad'] == 'LL') { echo "selected=''"; } ?>>Lain-lain</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">timeline</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Status Hidup Ayah <small>(required)</small></label>
                                        <select name="life_statdad" id="" class="form-control">
                                            <option value="MH" <?php if($detl['life_statdad'] == 'MH') { echo "selected=''"; } ?>>Masih Hidup</option>
                                            <option value="SM" <?php if($detl['life_statdad'] == 'SM') { echo "selected=''"; } ?>>Sudah Meninggal</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- detail ayah /end -->
                            <hr>
                            <!-- detail ibu -->
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nama Ibu <small>(required)</small></label>
                                        <input class="form-control" type="text" name="nm_ibu" value="<?php echo strtoupper($detl['nm_ibu']); ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">school</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pendidikan Ibu <small>(required)</small></label>
                                        <select name="didik_ibu" id="" class="form-control">
                                            <option value="NSD" <?php if($detl['didik_ibu'] == 'NSD') { echo "selected=''"; } ?>>Tidak tamat SD</option>
                                            <option value="YSD" <?php if($detl['didik_ibu'] == 'YSD') { echo "selected=''"; } ?>>Tamat SD</option>
                                            <option value="SMP" <?php if($detl['didik_ibu'] == 'SMP') { echo "selected=''"; } ?>>Tamat SLTP</option>
                                            <option value="SMA" <?php if($detl['didik_ibu'] == 'SMA') { echo "selected=''"; } ?>>Tamat SLTA</option>
                                            <option value="DPL" <?php if($detl['didik_ibu'] == 'DPL') { echo "selected=''"; } ?>>Diploma</option>
                                            <option value="SMD" <?php if($detl['didik_ibu'] == 'SMD') { echo "selected=''"; } ?>>Sarjana Muda</option>
                                            <option value="SRJ" <?php if($detl['didik_ibu'] == 'SRJ') { echo "selected=''"; } ?>>Sarjana</option>
                                            <option value="PSC" <?php if($detl['didik_ibu'] == 'PSC') { echo "selected=''"; } ?>>Pascasarjana</option>
                                            <option value="DTR" <?php if($detl['didik_ibu'] == 'DTR') { echo "selected=''"; } ?>>Doctor</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">work</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pekerjaan Ibu <small>(required)</small></label>
                                        <select name="workmom" id="" class="form-control">
                                            <option value="PN" <?php if($detl['workmom'] == 'PN') { echo "selected=''"; } ?>>Pegawai Negeri</option>
                                            <option value="TP" <?php if($detl['workmom'] == 'TP') { echo "selected=''"; } ?>>TNI / POLRI</option>
                                            <option value="PS" <?php if($detl['workmom'] == 'PS') { echo "selected=''"; } ?>>Pegawai Swasta</option>
                                            <option value="WU" <?php if($detl['workmom'] == 'WU') { echo "selected=''"; } ?>>Wirausaha</option>
                                            <option value="PE" <?php if($detl['workmom'] == 'PE') { echo "selected=''"; } ?>>Pensiun</option>
                                            <option value="TK" <?php if($detl['workmom'] == 'TK') { echo "selected=''"; } ?>>Tidak Bekerja</option>
                                            <option value="LL" <?php if($detl['workmom'] == 'LL') { echo "selected=''"; } ?>>Lain-lain</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">timeline</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Status Hidup Ibu <small>(required)</small></label>
                                        <select name="life_statmom" id="" class="form-control">
                                            <option value="MH" <?php if($detl['life_statmom'] == 'MH') { echo "selected=''"; } ?>>Masih Hidup</option>
                                            <option value="SM" <?php if($detl['life_statmom'] == 'SM') { echo "selected=''"; } ?>>Sudah Meninggal</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">attach_money</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Jumlah Penghasilan Orang Tua <small>(required)</small></label>
                                        <input type="radio" value="1" <?php if($detl['penghasilan'] == '1') { echo 'checked="checked"'; } ?> name="gaji" required> Rp 1,000,000 - 2,000,000 &nbsp;&nbsp;
                                    
                                        <input type="radio" value="2" <?php if($detl['penghasilan'] == '2') { echo 'checked="checked"'; } ?> name="gaji"> Rp 2,100,000 - 4,000,000 <br>
                                    
                                        <input type="radio" value="3" <?php if($detl['penghasilan'] == '3') { echo 'checked="checked"'; } ?> name="gaji"> Rp 4,100,000 - 5,999,000  &nbsp;&nbsp;
                                
                                        <input type="radio" value="4" <?php if($detl['penghasilan'] == '4') { echo 'checked="checked"'; } ?> name="gaji"> >= Rp 6,000,000
                                    </div>
                                </div>
                            </div>
                            <!-- detail ibu /end -->
                        </div>
                    </div>

                    <!-- kelengkapan data -->
                    <div class="tab-pane" id="address">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">credit_card</i>
                                    </span>
                                    <div class="form-group">
                                        <label class="control-label">Pengguna BPJS <small>(required)</small></label><br> 
                                        <input type="radio" id="bpjs-y" name="bpjs" <?php if ($detl['bpjs'] == 'y') { echo 'checked="checked"'; } ?> value="y" required> Ya &nbsp;&nbsp;
                                        <input type="radio" id="bpjs-n" name="bpjs" <?php if ($detl['bpjs'] == 'n') { echo 'checked="checked"'; } ?> value="n" > Tidak &nbsp;&nbsp; 
                                        <input class="form-control" id="bpjs-yes" type="text" placeholder="Nomor BPJS" value="<?php echo $detl['no_bpjs']; ?>" name="nobpjs">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">motorcycle</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Alat Transportasi <small>(required)</small></label>
                                        <select name="transportasi" id="" class="form-control">
                                            <option value="MBL" <?php if($detl['transport'] == 'MBL') { echo "selected=''"; } ?>>Mobil</option>
                                            <option value="MTR" <?php if($detl['transport'] == 'MTR') { echo "selected=''"; } ?>>Motor</option>
                                            <option value="AKT" <?php if($detl['transport'] == 'AKT') { echo "selected=''"; } ?>>Angkutan Umum</option>
                                            <option value="SPD" <?php if($detl['transport'] == 'SPD') { echo "selected=''"; } ?>>Sepeda</option>
                                            <option value="JKK" <?php if($detl['transport'] == 'JKK') { echo "selected=''"; } ?>>Jalan Kaki</option>
                                            <option value="ADG" <?php if($detl['transport'] == 'ADG') { echo "selected=''"; } ?>>Andong</option>
                                            <option value="KRT" <?php if($detl['transport'] == 'KRT') { echo "selected=''"; } ?>>Kereta</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">question_answer</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Referensi Memilih UBJ <small>(required)</small></label>
                                        <input class="form-control" id="" type="text" name="refer" value="<?php echo $detl['referensi']; ?>" />
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" value="<?php echo $detl['key_booking']; ?>" name="kunci">
                            <input type="hidden" value="<?php echo $detl['user_input']; ?>" name="log">
                            <input type="hidden" value="<?php echo $detl['gelombang']; ?>" name="gelombang">
                            <!-- <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">assignment</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Surat Kelengkapan <small>(required)</small></label>
                                        <div class="dda" id="akt"><input type="checkbox" name="lengkap[]" value="AKT"> Akte Kelahiran &nbsp;&nbsp;</div>
                                        <div class="dda" id="kk"><input type="checkbox" name="lengkap[]" value="KK"> Kartu Keluarga (KK) &nbsp;&nbsp; </div>
                                        <div class="dda" id="ktp"><input type="checkbox" name="lengkap[]" value="KTP"> Kartu Tanda Penduduk (KTP)  &nbsp;&nbsp;</div>
                                        <div class="dda" id="rpt"><input type="checkbox" name="lengkap[]" value="RP"> Rapot  &nbsp;&nbsp;</div>
                                        <div class="dda" id="skhun"><input type="checkbox" name="lengkap[]" value="SKHUN"> SKHUN  </div>
                                        <div class="dda" id="foto"><input type="checkbox" name="lengkap[]" value="FT"> Foto (3x4 dan 4x6) &nbsp;&nbsp;</div>
                                        <div class="dda" id="ijz"><input type="checkbox" name="lengkap[]" value="IJZ"> Ijazah &nbsp;&nbsp; </div>
                                        <div class="dda" id="skl"><input type="checkbox" name="lengkap[]" value="SKL"> Surat Kelulusan  &nbsp;&nbsp;<br></div>
                                        
                                        <div id="spd"><input type="checkbox" name="lengkap[]" value="SPD"> Surat Pindah &nbsp;&nbsp;</div>
                                        <div id="sak"><input type="checkbox" name="lengkap[]" value="SAK"> Sertifikat Akreditasi &nbsp;&nbsp;</div>

                                        <div id="tkr"><input type="checkbox" name="lengkap[]" value="TKR"> Transkrip  &nbsp;&nbsp;</div>
                                        <div id="baa"><input type="checkbox" name="lengkap[]" value="LBAA"> Laporan BAA  &nbsp;&nbsp;</div>
                                        <div id="ketren"><input type="checkbox" name="lengkap[]" value="KTREN"> Keterangan RENKEU </div>

                                        <div class="dda" id="lkp"><input type="checkbox" name="lengkap[]" value="LLKP"> Lengkap Administratif  &nbsp;&nbsp;</div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="wizard-footer">
                    <div class="pull-right">
                        <input type='button' class='btn btn-next btn-fill btn-success btn-wd' name='next' value='Next' />
                        <input type='submit' id="simpan" class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='Finish' />
                    </div>

                    <div class="pull-left">
                        <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' />
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div> <!-- wizard container -->
    <br><br>
    
</div>

<!-- <div class="col-md-12 alert alert-warning alert-dismissible fade in" role="alert">
    <strong>Jika data anda telah sesuai, anda dapat melanjutkan ke menu <a href="<?php // echo base_url('dashboard/berkas') ?>" title="">kelengkapan berkas</a></strong>. 
    Atau apabila anda ingin merubah data pada formulir diatas, anda dapat <a href="<?php // echo base_url('dashboard/editForm/'.$detl['user_input'].'/'.$detl['prodi'].'/'.$detl['key_booking']); ?>" title="">klik disini</a>
</div> -->

    <!-- mask -->
    <!-- <script src="<?php //echo base_url('assets/wizard/dist/jQuery-mask/jquery.mask.js'); ?>"></script> -->
    <script src="<?php echo base_url('assets/wizard/dist/jQuery-mask/jquery.mask.min.js'); ?>"></script>

    <!-- wizard -->
    
    <script src="<?php echo base_url(); ?>assets/wizard/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/wizard/js/jquery.bootstrap.js" type="text/javascript"></script>
    <!--  Plugin for the Wizard -->
    <script src="<?php echo base_url(); ?>assets/wizard/js/material-bootstrap-wizard.js"></script>
    <!--  More information about jquery.validate here: http://jqueryvalidation.org/  -->
    <script src="<?php echo base_url(); ?>assets/wizard/js/jquery.validate.min.js"></script>

            
            
  <!-- <script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script> -->
            
    
    </body>
</html>