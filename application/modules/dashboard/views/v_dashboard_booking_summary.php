<!--start page content-->
<?php 
    $sesi = $this->session->userdata('sess_login_pmb');
    $book = $this->session->userdata('sess_booking');
    $keys = $this->session->userdata('sess_keyorder');
 ?>
<div class="row">
<div class="col-md-2">
</div>
<div class="col-md-8">
        <div class="panel panel-warning collapsed">
            <div class="panel-heading">
                Data Pemesanan Formulir
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                <tbody>
                <tr>
                    <td>Email</td>
                    <td><?php echo $sesi['username']; ?></td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td><?php echo getName($sesi['userid']); ?></td>
                </tr>
                <tr>
                    <td>Program Studi</td>
                    <td><?php echo get_prodi($book['opsiprodi']); ?></td>
                </tr>
                <tr>
                    <td>Lokasi Kampus</td>
                    <td> <?php echo getCamp($book['kampus']); ?> </td>
                </tr>
                <tr>
                    <td>Gelombang</td>
                    <td><?php echo $book['gelombang'] ?></td>
                </tr>
                <tr>
                    <td>Booking ID</td>
                    <td><?php echo substr($keys,0,11); ?></td>
                </tr>
                <tr>
                    <td>Total</td>
                    <td><?php if($book['program'] == 1) { echo 'Rp. 300.000,-';} else { echo 'Rp. 400.000,-';} ?> </td>
                </tr>
                </tbody>
            </table>
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <center>
                <a href="<?php echo base_url('dashboard'); ?>"><button class="btn btn-info btn-rounded"><i class="fa fa-home"></i>&nbsp;&nbsp; Kembali ke Dashboar</button></a>
                <a href="<?php echo base_url('dashboard/booking_form/printBook/'.substr($keys,0,11)) ?>" target="_blank"><button class="btn btn-success btn-rounded"><i class="fa fa-print"></i>&nbsp;&nbsp; Cetak</button></a>
                </center>
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div>
</div><!--end row-->