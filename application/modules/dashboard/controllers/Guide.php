<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guide extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login_pmb') != TRUE) {
			echo '<script>alert("Silahkan log-in kembali!");</script>';
			redirect(base_url('auth/login/out'),'refresh');
		}
	}
	
	public function index()
	{
		$data['page'] = 'v_guide';
		$this->load->view('v_template_dashboard', $data);
	}

}

/* End of file Guide.php */
/* Location: ./application/modules/dashboard/controllers/Guide.php */