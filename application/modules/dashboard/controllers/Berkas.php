<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berkas extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$ses = $this->session->userdata('sess_login_pmb');
		if ($ses != TRUE) {
			echo "<script>alert('Anda telah kehabisan sesi login. Silahkan log-in kembali!');</script>"; exit();
			redirect(base_url('auth/login/out'));
		}
		$this->load->helper('cond_helper');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		$sess = $this->session->userdata('sess_login_pmb');
		/** $arr = array('userid' => $sess['userid'], 'valid' => 2);
        $getDet = $this->crud_model->getMoreWhere('tbl_booking',$arr)->num_rows();
        if ($getDet > 1) {
        	echo '<script>alert("Anda memesan formulir lebih dari satu. Mohon batalkan salah satu pemesanan untuk dapat melanjutkan proses pemenuhan berkas.");history.go(-1)</script>';
        	exit();	
        } */
        $usr = $this->crud_model->getDetail('tbl_form_pmb','user_input',$sess['userid']);
		if ($usr->num_rows() < 1) {
			echo "<script>alert('Mohon lakukan pengisian formulir terlebih dahulu!');history.go(-1);</script>"; exit();
		} elseif ($usr->num_rows() == 1) {
			$this->saveSessFile($usr->row()->key_booking);
		} elseif ($usr->num_rows() > 1) {
			$data['forkey'] = $usr->result();
			$data['page'] = 'modal_askfor_berkas';
			$this->load->view('v_template_dashboard', $data, FALSE);
		}
	}

	function postFormModalFile()
	{
		$catch = $this->input->post('optprodi'); //var_dump($catch);exit();
		redirect(base_url('dashboard/berkas/saveSessFile/'.$catch));
	}

	function basecont()
	{
		$key = $this->session->userdata('sess_for_file');
		$data['arrkey'] = $key;
		$sess = $this->session->userdata('sess_login_pmb');
		$arr = ['user_input' => $sess['userid'], 'key_booking' => $key];
		$data['filt'] = $this->crud_model->getMoreWhere('tbl_form_pmb',$arr)->row();
		$data['page'] = 'v_dashboard_berkas';
		$this->load->view('v_template_dashboard',$data);		
	}

	function saveSessFile($key)
	{
		if ($this->session->userdata('sess_for_file') == TRUE) {
			$this->session->unset_userdata('sess_for_file');
		}
		$this->session->set_userdata('sess_for_file', $key);
		redirect(base_url('dashboard/berkas/basecont'));
	}

	function get_berkas()
	{
		$sess = $this->session->userdata('sess_login_pmb');
		$data = $this->crud_model->getdetail('tbl_form_pmb','user_input',$sess['userid'])->row()->jenis_pmb;
		$parse = json_encode($data);
		echo $parse;
	}

	function fileIn()
	{
		$usr = $this->input->post('user');
		$typ = $this->input->post('tipe');
		$key = $this->input->post('key');

		// var_dump($_FILES['userfile']);exit();

		if ($_FILES['userfile']) {
			$this->load->helper('inflector');
			$sanitize = $this->security->sanitize_filename($_FILES['userfile']['name']);
			$nama = underscore(str_replace('/', '', $sanitize));

			if ($_FILES['userfile']['size'] > 2048000) {
				echo "<script>alert('UKURAN FILE TIDAK BOLEH MELEBIHI 2 MB!');history.go(-1);</script>"; exit();
			} else {
				$kadal = explode('.', $nama);
				if (count($kadal) > 2) {
					echo "<script>alert('FORMAT FILE MENCURIGAKAN!');history.go(-1);</script>";exit();
				} 

	            $config['allowed_types'] = 'jpg|jpeg|png|pdf';
	            $config['max_size'] = '10240';
	            $config['file_name'] = $usr.$nama;
	            $config['upload_path'] = './file_image/';

	            $pth = substr($config['upload_path'],1).$usr.$nama;

	            $this->load->library('upload', $config);
	            if (!$this->upload->do_upload("userfile")) {
	            	$error = array('Format atau ukuran file yang anda unggah tidak sesuai. Silahkan ulang kembali proses pengunggahan anda.');
	                /* $error = array('error' => $this->upload->display_errors()); */ var_dump($error);
	            } else {
	                $data = array(
						'userid'	=> $usr,
						'path'		=> $pth,
						'tipe'		=> $typ,
						'tgl'		=> date('Y-m-d h:i:s'),
						'valid'		=> 2,
						'key_booking'	=> $key
						);
					// var_dump($data);exit();
	                $arr = array('userid' => $usr, 'tipe' => $typ, 'key_booking' => $key);
	                $avb = $this->crud_model->getMoreWhere('tbl_file',$arr)->num_rows();
					if ($avb > 0) {
						unlink(base_url().$avb->path);
						$obj = array('path' => $pth, 'tgl' => date('Y-m-d h:i:s'), 'valid' => 2, 'key_booking' => $key);
						$this->db->where('userid', $usr)->where('tipe',$typ)->update('tbl_file',$obj);
					} else {
						// var_dump($data);exit();
						$this->crud_model->insertData('tbl_file',$data);
					}	
						
					redirect(base_url('dashboard/berkas/basecont'));
	            }
			}
        }
	}

	function lihatberkas($uid, $type, $keybook)
	{
		$this->db->select('path');
		$this->db->from('tbl_file');
		$this->db->where('userid', $uid);
		$this->db->where('tipe', $type);
		$this->db->where('key_booking', $keybook);
		$data['foto'] = $this->db->get();
		$this->load->view('modallihatberkas', $data);
	}

}

/* End of file Berkas.php */
/* Location: ./application/modules/dashboard/controllers/Berkas.php */