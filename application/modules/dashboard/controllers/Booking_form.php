<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booking_form extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('sess_login_pmb') != TRUE) {
			echo '<script>alert("Silahkan log-in kembali!");</script>';
			redirect(base_url('auth/login/out'),'refresh');
		}
		$this->dbsia = $this->load->database('sia', TRUE);
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		$this->dbsia = $this->load->database('sia',TRUE);
		$this->load->helper('cond_helper');
		$data['prodi'] = $this->dbsia->get('tbl_fakultas')->result();
		$data['page'] = 'v_dashboard_booking';
		$this->load->view('v_template_dashboard',$data);
	}

	function save_booking()
	{
		$usr 	= $this->session->userdata('sess_login_pmb');
		$camp 	= $this->input->post('kampus');
		$prodi 	= $this->input->post('prodi');
		$prod2 	= $this->input->post('prodi1');
		$prod3 	= $this->input->post('prodi2');
		$prod4 	= $this->input->post('prodi3');
		$jenjang= $this->input->post('jenjang');
		$program= $this->input->post('program');

		$quota 	= $this->dbsia->where('kd_prodi', $prodi)
							  ->where('status_kuota', 1)
							  ->get('tbl_gel_kuotaprodi')->num_rows();

		$gel 	= $this->dbsia->where('status',1)->get('tbl_gelombang_pmb')->row()->gelombang;
		if ($quota > 0) {
			// make booking session
			$qey = $this->generateKodeBooking();

			$exist = $this->db->where('key', $qey)->get('tbl_booking')->num_rows();
			while ($exist > 0) {
				$qey = $this->generateKodeBooking();
			}
			
			$array = array(
				'program'	=> $jenjang,
				'opsiprodi' => $prodi,
				'gelombang'	=> $gel,
				'kampus'	=> $camp
			);
			$this->session->set_userdata('sess_booking', $array);
			// make booking session end

			// insert to tbl_booking
			$in = array(
				'userid'	=> $usr['userid'],
				'key'		=> $qey,
				'prodi'		=> $prodi,
				'prodi2'	=> $prod2,
				'prodi3'	=> $prod3,
				'prodi4'	=> $prod4,
				'gel'		=> $gel,
				'camp'		=> $camp,
				'bookdate'	=> date('ymd'),
				'program'	=> $jenjang,
				'jenisreg'	=> $program
			);

			/*
			* jika sudah melakukan pemesanan
			* cek jenis program, prodi yang dipilih, dan gelombang pendaftaran
			* apabila ada kesamaan di ketiganya maka proses pemesanan akan ditolak.
			* data yang diinput akan disimpan dalam session "sesi_pemesanan"
			*/
			if ($this->session->userdata('sesi_pemesanan')) {

				$cek1 = $this->db->where('userid', $usr['userid'])
							->where('prodi', $prodi)
							->where('gel',$gel)
							->group_start()
							->where('valid !=', '3')
							->or_group_start()
							->where('valid', NULL)
							->group_end()
							->group_end()
							->get('tbl_booking')->num_rows();

				if ($cek1 > 0) {
					echo "<script>alert('Anda hanya dapat mendaftar satu kali pada satu periode gelombang!');history.go(-1);location.reload()</script>"; exit();
				} else {
					$booked = $this->session->userdata('sesi_pemesanan');
					if ($booked['userid']==$usr['userid'] && $booked['prodi']==$prodi && $booked['gel']==$gel) {
						echo "<script>alert('Anda hanya dapat mendaftar satu kali pada satu periode gelombang!');history.go(-1);location.reload()</script>"; exit();
					} else {
						$this->session->unset_userdata('sesi_pemesanan');
						$this->session->set_userdata('sesi_pemesanan', $in);
					}	
				}
			} else {
				// cek apakah sudah booking dgn tipe sama
				$cek = $this->db->where('userid', $usr['userid'])
							->where('prodi', $prodi)
							->where('gel',$gel)
							->group_start()
							->where('valid !=', '3')
							->or_group_start()
							->where('valid', NULL)
							->group_end()
							->group_end()
							->get('tbl_booking')->num_rows();

				if ($cek > 0) {
					echo "<script>alert('Anda hanya dapat mendaftar satu kali pada satu periode gelombang!');history.go(-1);location.reload()</script>"; exit();
				} else {
					$this->session->set_userdata('sesi_pemesanan', $in);
				}
			}

			// buat sesi untuk key order
			$this->session->set_userdata('sess_keyorder', $qey);
			$data['page'] = 'v_dashboard_payguide';
			$this->load->view('v_template_dashboard',$data);	
		} else {
			echo "<script>alert('Pendaftaran untuk prodi ".get_prodi($prodi)." sudah memenuhi kuota maksimal!');history.go(-1);</script>";
		}	
	}

	function generateKodeBooking()
	{
		$hash = NULL;
        $n = 16; // jumlah karakter yang akan di bentuk.
        $chr = "0123456789";
        for ($i = 0; $i < $n; $i++) {
            $rIdx = rand(1, strlen($chr));
            $hash .=substr($chr, $rIdx, 1);
        }
        $key = $hash;
        return $key;
	}

	function summary()
	{
		$pesanan = $this->session->userdata('sesi_pemesanan');
		// update field ready
		$oke = $this->input->post('oke');
		$usr = $this->input->post('user');
		$key = $this->input->post('keyy');
		$arr = array('ready' => $oke);

		$data = [
			'userid'	=> $usr,
			'key'		=> $key,
			'prodi'		=> $pesanan['prodi'],
			'prodi2'	=> $pesanan['prodi2'],
			'prodi3'	=> $pesanan['prodi3'],
			'prodi4'	=> $pesanan['prodi4'],
			'gel'		=> $pesanan['gel'],
			'camp'		=> $pesanan['camp'],
			'bookdate'	=> $pesanan['bookdate'],
			'program'	=> $pesanan['program'],
			'jenisreg'	=> $pesanan['jenisreg'],
			'ready'		=> $oke
		];

		$this->db->insert('tbl_booking', $data);

		/* update
		$this->db->where('userid', $usr);
		$this->db->where('key', $key);
		$this->db->update('tbl_booking', $arr);
		*/

		// $this->session->unset_userdata('sesi_pemesanan');
				
		$data['page'] = 'v_dashboard_booking_summary';
		$this->load->view('v_template_dashboard',$data);	
	}

	function voidBook($key)
	{
		$ses = $this->session->userdata('sess_login_pmb');
		if ($key == '' || !$key) {
			$key = $this->input->post('optprodi');
		}
		// update tbl_booking
		$arr = array('valid' => 3);
		$this->crud_model->updateData('tbl_booking','key',$key,$arr);

		/*
		* update tbl_form_pmb
		* if he/she has filled the form then check tbl_form_pmb to update
		* his/her status_form
		*/
		$arrt = ['user_input' => $ses['userid'], 'key_booking' => $key];
		$cekform = $this->crud_model->getMoreWhere('tbl_form_pmb',$arrt)->num_rows();
		if ($cekform == 1) {
			$arrk = array('user_input' => $ses['userid'], 'prodi' => $key);
			$data = array('status_form' => 2);
			$this->crud_model->updateMoreWhere('tbl_form_pmb',$arrk,$data);	
		} else {
			redirect(base_url('dashboard'));
		}
	}

	function loadModalPayment($key)
	{
		$data['forkey'] = $this->crud_model->getDetail('tbl_booking','key',$key)->row();
		$this->load->view('modalpayment', $data);
	}

	function payConfirm()
	{
		$sess = $this->session->userdata('sess_login_pmb');
		$nmrek = $this->input->post('nama_rek');
		$tpay = $this->input->post('paytip');
		$ths = $this->input->post('key');

		// explode bank post
		$bank = $this->input->post('bank');
		$one = explode(' ', $bank);
		$two = $one[0];

		$norek = $this->input->post('no_rek');
		if ($_FILES['userfile']) {
			$this->load->helper('inflector');
			$nama = underscore(str_replace('/', '', $this->security->sanitize_filename($_FILES['userfile']['name'])));

			// var_dump($_FILES["userfile"]["name"]);exit();

			// if ($_FILES['userfile']['size'] > 20480) {

			// 	echo "<script>alert('UKURAN FILE TIDAK BOLEH MELEBIHI 2 MB!');history.go(-1);</script>"; 
			// 	exit();

			// } else {

				$kadal = explode('.', $nama);
				if (count($kadal) > 2) {
					echo "<script>alert('FORMAT FILE MENCURIGAKAN!');history.go(-1);</script>";
					exit();
				} 

	            $config['allowed_types'] 	= 'jpg|jpeg|png';
	            $config['max_size'] 		= '10240';
	            $config['file_name'] 		= $ths.$nama;
	            $config['upload_path'] 		= './rek_image/';

	            $path = substr($config['upload_path'],1).$ths.$nama;

	            $this->load->library('upload', $config);
	            if (!$this->upload->do_upload("userfile")) {
	                $error = array('error' => $this->upload->display_errors());
	                var_dump($error);
	            } else {
	            	if ($tpay == '1') {
	            		$data = array(
							'norek'		=> $norek,
							'nmrek'		=> $nmrek,
							'bank'		=> $two,
							'patch'		=> $path,
							'userid'	=> $sess['userid'],
							'datepay'	=> date('ymd'),
							'paytipe'	=> $tpay,
							'key_booking'	=> $ths
							);	
	            	} else {
	            		$data = array(
							'patch'		=> $path,
							'userid'	=> $sess['userid'],
							'datepay'	=> date('ymd'),
							'paytipe'	=> $tpay,
							'key_booking'	=> $ths
							);
	            	}
					// var_dump($data);exit();
					$this->crud_model->insertData('tbl_payment',$data);

					// update validation on tbl_booking
					$arr = array('userid' => $sess['userid'], 'key' => $ths);
					$data = array('valid' => 1);
	        		$this->crud_model->updateMoreWhere('tbl_booking',$arr,$data);
	        		
					echo "<script>alert('Berhasil');history.go(-1);</script>";
	            }
        	// }
        }
	}

	function printBook($k)
	{
		$this->load->library('Cfpdf');
		$data['load'] = $this->crud_model->getDetail('tbl_booking','key',$k)->row();
		$this->load->view('bukti_booking', $data);
	}

}

/* End of file Booking_form.php */
/* Location: ./application/controllers/Booking_form.php */